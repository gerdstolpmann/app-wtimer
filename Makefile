
.PHONY: all opt clean install

all:
	cd static && $(MAKE) all
	cd src && $(MAKE) all

opt:
	cd static && $(MAKE) opt
	cd src && $(MAKE) opt

clean:
	cd static && $(MAKE) clean
	cd src && $(MAKE) clean

install:
	cd static && $(MAKE) INST_PREFIX="$(INST_PREFIX)" install
	cd src && $(MAKE) INST_PREFIX="$(INST_PREFIX)" install
	cd ddl && $(MAKE) INST_PREFIX="$(INST_PREFIX)" install
