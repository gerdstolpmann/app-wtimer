======================================================================
Directory layout after installation:
======================================================================

<ETC_DIR>/wtimer-config.xml
<WTIMER_DIR>/wef.dtd
<WTIMER_DIR>/ui/*.ui
<WTIMER_DIR>/ui/wtimer-config.xml -> <ETC_DIR>/wtimer-config.xml
<WTIMER_DIR>/static/*.{png,css}
<WTIMER_DIR>/ddl/*
<LIB_DIR>/wtimer/cgi/wtimer.cgi                 (executable)
<BIN_DIR>/wtimer-admin                          (executable)
<SBIN_DIR>/wtimerd                              (executable)

======================================================================
URL layout (application server mode)
======================================================================

<srvprefix>/wtimer              => connected with application server
<srvprefix>/wtimer-images       => connected with "images" directory

======================================================================
URL layout (CGI mode)
======================================================================

<cgiprefix>/wtimer.cgi          => connected with wtimer.cgi
<cgiprefix>/wtimer-images       => connected with "images" directory

