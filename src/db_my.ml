(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


(* THIS MODULE IS UNMAINTAINED! *)

(* Access MySQL by ocamlodbc: *)

module Connection = struct

  include Db_pg.Connection
    (* This module bases on Db_pg and redefines some functions *)

  open Printf

  let _ = 
    _begin_transaction := (fun c -> 
			     _exec_nr c "set session transaction isolation level read committed";
			     _exec_nr c "set autocommit=0";
			     _exec_nr c "begin")


  let have_on_delete_cascade = false		     

  let have_time_keyword = ""

  let have_interval_keyword = ""

  let day_interval d =
    sprintf "interval %d day" d

  let quote_re = Netstring_str.regexp "[\\']"

  let quote s =
    Netstring_str.global_replace quote_re "\\\\\\0" s

  let bool_true = "1"
  let bool_false = "0"

  let is_bool_true =
    function
	"1" | "t" | "true" | "y" | "yes" -> true
      | _   -> false

  let is_bool_false =
    function
	"0" | "f" | "false" | "n" | "no" -> true
      | _   -> false

  let expr_next_serial_value table column =
    (* Return the SQL expression to increment the counter for [table] and
     * [column], and to return the new value.
     *)
    "0"

  let expr_last_serial_value table column =
    (* Return the SQL expression to return the counter for [table] and
     * [column] again
     *)
    "last_insert_id()"

  let get_last_serial_value c table column =
    let l = exec c (sprintf "select %s" 
		      (expr_last_serial_value table column)) in
    match l with
	[[ r ]] -> int_of_string r
      | _ -> failwith "get_last_serial_value"

end
