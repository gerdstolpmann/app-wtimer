(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

module Connection = Db_DBVARIANT.Connection
		      (* Db_pg.Connection for PostgreSQL
		       * Db_my.Connection for MySQL
		       *)

module SSet = Set.Make(String)

let config =
  Get_config.parse()

module Types = struct
  open Db_types.Types

  type user_name = User of string

  type user = { u_name : user_name;
		u_description : string;
		u_password : string option;
		u_login : bool;
		u_admin : bool;
                u_email : string;
                u_managers : user_name list;
	      }

  exception No_such_user of user_name
  exception User_already_exists of user_name

  type inst_name = Instance of string

  type instance = { i_name : inst_name;
		    i_description : string;
		  }

  exception No_such_instance of inst_name
  exception Instance_already_exists of inst_name

  type perm_type = [ `Write | `Read | `Owner ]

  type perm_set = { p_instance : inst_name;
		    p_set : (user_name * perm_type) list;
		  }

  type approval =
    { a_instance : inst_name;
      a_year : int;
      a_month : int;
      a_approved : (timestamp * user_name) option;
    }

  type entry = { mutable e_id : int option;
		 e_instance : inst_name;
		 e_day : date;
		 e_index : int;
		 e_start : time option;
		 e_end : time option;
		 e_duration : interval option;
		 e_project : string;
		 e_description : string;
	       }

  type day = { d_instance : inst_name;
	       d_day : date;
	       d_entries : entry list;
	     }

  type 't change =
    { mc_timestamp : int64;   (* microseconds since the epoch *)
      mc_login : user_name;
      mc_change : 't;
    }

  let string_of_user_name (User s) = s
  let string_of_inst_name (Instance s) = s

end


module User = struct
  open Types
  open Db_types.Types
  open Db_types
  open Connection
  open Printf

  let list c =
    let l = exec c "select name,description,password IS NULL,password,login,admin,email,managers from users" in
    List.map
      (fun row ->
	 match row with
           | [n; d; p_null; p; l; a; e; m] ->
               let managers =
                 String.split_on_char ',' m
                 |> List.filter (fun s -> s <> "")
                 |> List.map (fun s -> User s) in
	       { u_name = User n;
		 u_description = d;
		 u_password = if is_bool_true p_null then None else Some p;
		 u_login = is_bool_true l;
                 u_admin = is_bool_true a;
                 u_email = e;
                 u_managers = managers;
	       }
	   | _ ->
	       failwith "Db.User.list: Unexpected return list of SELECT"
      )
      l


  let get c (User n) =
    let l = exec c
              (sprintf "select description,password IS NULL,password,login,admin,email,managers from users where name = '%s'" (quote n)) in
    match l with
	[] ->
	  raise (No_such_user (User n))
      | [row] ->
	  ( match row with
              | [d; p_null; p; l; a; e; m] ->
                  let managers =
                    String.split_on_char ',' m
                    |> List.filter (fun s -> s <> "")
                    |> List.map (fun s -> User s) in
		  { u_name = User n;
		    u_description = d;
		    u_password = if is_bool_true p_null then None else Some p;
		    u_login = is_bool_true l;
		    u_admin = is_bool_true a;
                    u_email = e;
                    u_managers = managers;
		  }
	      | _ ->
		  failwith "Db.User.get: Unexpected return list of SELECT"
	  )
      | _ ->
	  failwith "Db.User.get: Unexpected return list of SELECT"


  let exists c uname =
    let l = exec c
	      (sprintf "select count(*) from users where name = '%s'"
		 (quote (string_of_user_name uname))) in
    match l with
	[[ n ]] ->
	  int_of_string n > 0
      | _ ->
	  failwith "Db.User.exists: Unexpected return list of SELECT"


  let insert c u =
    if exists c u.u_name then raise (User_already_exists u.u_name);
    exec_nr c
      (sprintf "insert into users (name,description,password,login,admin,email,managers) values ('%s', '%s', %s, %s, %s, '%s', '%s')"
	 (quote (string_of_user_name u.u_name))
	 (quote u.u_description)
	 (match u.u_password with
	      None -> "NULL"
	    | Some pw -> "'" ^ quote pw ^ "'")
	 (if u.u_login then bool_true else bool_false)
         (if u.u_admin then bool_true else bool_false)
         (quote u.u_email)
         ( u.u_managers
           |> List.map string_of_user_name
           |> String.concat ","
           |> quote
         )
      )

  let update c u =
    if not (exists c u.u_name) then raise (No_such_user u.u_name);
    exec_nr c
      (sprintf "update users set description = '%s', password = %s, login = %s, admin = %s, email = '%s', managers = '%s' where name = '%s'"
	 (quote u.u_description)
	 (match u.u_password with
	      None -> "NULL"
	    | Some pw -> "'" ^ quote pw ^ "'")
	 (if u.u_login then bool_true else bool_false)
	 (if u.u_admin then bool_true else bool_false)
         (quote u.u_email)
         ( u.u_managers
           |> List.map string_of_user_name
           |> String.concat ","
           |> quote
         )
	 (quote (string_of_user_name u.u_name)))

  let delete c uname =
    if not (exists c uname) then raise (No_such_user uname);
    exec_nr c
      (sprintf "delete from users where name = '%s'"
	 (quote (string_of_user_name uname)));
    if not have_on_delete_cascade then begin
      exec_nr c
	(sprintf "delete from permission where username = '%s'"
	   (quote (string_of_user_name uname)));
    end



  let encrypt_password pw =
    Digest.to_hex (Digest.string pw)

end

module Instance = struct
  open Types
  open Db_types.Types
  open Db_types
  open Connection
  open Printf

  let list c =
    let l = exec c "select name,description from instance" in
    List.map
      (fun row ->
	 match row with
	     [n; d] ->
	       { i_name = Instance n;
		 i_description = d
	       }
	   | _ ->
	       failwith "Db.Instance.list: Unexpected return list of SELECT"
      )
      l

  let get c iname =
    let l = exec c
	      (sprintf "select description from instance where name = '%s'"
		 (quote (string_of_inst_name iname))) in
    match l with
	[] -> raise (No_such_instance iname)
      | [[d]] ->
	  { i_name = iname;
	    i_description = d
	  }
      | _ ->
	  failwith "Db.Instance.get: Unexpected return list of SELECT"


  let exists c iname =
    let l = exec c
	      (sprintf "select count(*) from instance where name = '%s'"
		 (quote (string_of_inst_name iname))) in
    match l with
	[[ n ]] ->
	  int_of_string n > 0
      | _ ->
	  failwith "Db.Instance.exists: Unexpected return list of SELECT"


  let insert c i =
    if exists c i.i_name then raise (Instance_already_exists i.i_name);
    exec_nr c
      (sprintf "insert into instance (name,description) values ('%s', '%s')"
	 (quote (string_of_inst_name i.i_name))
	 (quote i.i_description))

  let update c i =
    if not (exists c i.i_name) then raise (No_such_instance i.i_name);
    exec_nr c
      (sprintf "update instance set description = '%s' where name = '%s'"
	 (quote i.i_description)
	 (quote (string_of_inst_name i.i_name)))

  let delete c iname =
    if not (exists c iname) then raise (No_such_instance iname);
    exec_nr c
      (sprintf "delete from instance where name = '%s'"
	 (quote (string_of_inst_name iname)));
    if not have_on_delete_cascade then begin
      exec_nr c
	(sprintf "delete from permission where instance = '%s'"
	   (quote (string_of_inst_name iname)));
      exec_nr c
	(sprintf "delete from entry where instance = '%s'"
	   (quote (string_of_inst_name iname)));
    end

end

module Approval = struct
  open Types
  open Db_types.Types
  open Db_types
  open Connection
  open Printf

  let enabled() =
    Get_config.bool_option config "approval-enable"

  let get c iname ~year ~month =
    let l =
      exec c (sprintf "select value,extract(epoch from approval_time),approval_user \
                       from approval
                       where instance = '%s'
                         and month='%04d-%02d-01'"
                      (quote (string_of_inst_name iname))
                      year
                      month) in
    match l with
      | [] ->
          { a_instance = iname;
            a_year = year;
            a_month = month;
            a_approved = None
          }
      | [ [ value; ts; user ] ] ->
          let timestamp = Timestamp.from_epoch_string ts in
          { a_instance = iname;
            a_year = year;
            a_month = month;
            a_approved = Some(timestamp, User user);
          }
      | _ ->
          failwith "Db.Approval.get: Unexpected return list of SELECT"

  let update c appr =
    match appr.a_approved with
      | None ->
          failwith "Db.Approval.update: cannot withdraw approval"
      | Some (timestamp, user) ->
          let old = get c appr.a_instance ~year:appr.a_year ~month:appr.a_month in
          if old.a_approved <> None then
            failwith "Db.Approval.update: cannot give approval for a second time";

          exec_nr c (sprintf "insert into approval \
                              (id, instance, month, value, approval_time, approval_user) \
                              values \
                              (%s, '%s', '%04d-%02d-01', 'A', to_timestamp(%s), '%s')"
                             (expr_next_serial_value "approval" "id")
                             (quote (string_of_inst_name appr.a_instance))
                             appr.a_year
                             appr.a_month
                             (Timestamp.to_epoch_string timestamp)
                             (quote (string_of_user_name user))
                    )

  let approve c login iname ~year ~month =
    let old = get c iname ~year ~month in
    if old.a_approved <> None then
      failwith "Db.Approval.approve: cannot give approval for a second time";
    exec_nr c (sprintf "insert into approval \
                        (id, instance, month, value, approval_time, approval_user) \
                        values \
                        (%s, '%s', '%04d-%02d-01', 'A', current_timestamp, '%s')"
                       (expr_next_serial_value "approval" "id")
                       (quote (string_of_inst_name iname))
                       year
                       month
                       (quote (string_of_user_name login))
              )
end

module Permission = struct
  open Types
  open Db_types.Types
  open Db_types
  open Connection
  open Printf

  let pt_map = [ `Read, "R";
		 `Write, "W";
		 `Owner, "O" ]

  let pt_rev = List.map (fun (a,b) -> (b,a)) pt_map

  let priority =
    function
	`Read -> 0
      | `Write -> 1
      | `Owner -> 2

  let check c iname uname pt =
    let query =
      (* Include all rights with same priority as pt or higher priority *)
      let pt_pri = priority pt in
      let rights =
	List.filter (fun (pt',_) -> priority pt' >= pt_pri) pt_map in
      String.concat
	" or "
	(List.map (fun (_,c) -> "type = '" ^ c ^ "'") rights)
    in

    let l = exec c
	      (sprintf "select count(*) from permission \
                        where instance = '%s' \
                          and username = '%s' \
                          and (%s)"
		 (quote (string_of_inst_name iname))
		 (quote (string_of_user_name uname))
		 query) in
    match l with
	[[ n ]] ->
	  int_of_string n > 0
      | _ ->
	  failwith "Db.Permission.check: Unexpected return list of SELECT"


  let get c iname =
    if not (Instance.exists c iname) then (raise (No_such_instance iname));
    let l = exec c
	      (sprintf "select username, type from permission \
                        where instance = '%s'"
		 (quote (string_of_inst_name iname))) in

    let l' = List.map
	       (fun e ->
		  match e with
		      [username; typ] ->
			let typ' =
			  try List.assoc typ pt_rev
			  with Not_found ->
			    failwith "Db.Permission.get: Unexpected return value"
			in
			(User username,typ')
		    | _ ->
			failwith "Db.Permission.get: Unexpected return list of SELECT"
	       )
	       l in
    (* l': List of tuples (username, [type]) *)

    let l'' = List.sort (fun (u,_) (v,_) -> compare u v) l' in
    (* l'': The same list sorted by username *)

    (* A function that makes l'' more dense: *)
    let rec compress p =
      match p with
	  (u1, t1) :: (u2, t2) :: p' when u1 = u2 ->
	    if priority t1 > priority t2 then
	      compress ((u1, t1) :: p')
	    else
	      compress ((u1, t2) :: p')
	| (u, t) :: p' ->
	    (u, t) :: compress p'
	| [] ->
	    []
    in

    { p_instance = iname;
      p_set = compress l''
    }

  let is_this_a_manager_of_the_owner_of_the_sheet c iname uname =
    let rec search visited user_name = (* also cover managers of managers *)
      let user_name_str = string_of_user_name user_name in
      if user_name_str = "" || SSet.mem user_name_str! visited then
        false
      else
        try
          let user = User.get c user_name in
          List.mem uname user.u_managers || (
            visited := SSet.add user_name_str !visited;
            List.exists
              (fun u -> search visited u)
              user.u_managers
          )
        with No_such_user _ -> false in
    let perm = get c iname in
    try
      let owner_name, _ =
        List.find (fun (_, pt) -> pt = `Owner) perm.p_set in
      search (ref SSet.empty) owner_name
    with
      | Not_found ->
          false


  let check_month_writable c iname uname ~year ~month =
    let approval = Approval.get c iname ~year ~month in
    check c iname uname `Write &&
      ( not(Approval.enabled()) ||
          approval.a_approved = None ||
            is_this_a_manager_of_the_owner_of_the_sheet c iname uname
      )


  let delete c iname =
    (* Internal function *)
    exec_nr c (sprintf "delete from permission where instance = '%s'"
		 (quote (string_of_inst_name iname)))


  let insert c iname p =
    (* Internal function *)
    List.iter
      (fun (user_name, right) ->
	 exec_nr c (sprintf "insert into permission \
                                    (instance,username,type) \
                                    values ('%s','%s','%s')"
		      (quote (string_of_inst_name iname))
		      (quote (string_of_user_name user_name))
		      (List.assoc right pt_map))
      )
      p

  let update c pset =
    if not (Instance.exists c pset.p_instance) then
      raise (No_such_instance pset.p_instance);
    delete c pset.p_instance;
    insert c pset.p_instance pset.p_set

end

module Entry = struct
  open Types
  open Db_types.Types
  open Db_types
  open Connection
  open Printf

  let list c iname start_date end_date =
    if not (Instance.exists c iname) then raise (No_such_instance iname);

    let l =
      exec c (sprintf "select distinct(day) from entry \
                       where instance = '%s' \
                         and day >= '%s' \
                         and day <= '%s'"
		(quote (string_of_inst_name iname))
		(Date.to_string start_date)
		(Date.to_string end_date)) in
    List.map
      (fun e ->
	 match e with
	     [ d ] ->
	       Date.from_string d
	   | _ ->
	       failwith "Db.Entry.list: Unexpected return list of SELECT"
      )
      l

  let projects c iname start_date end_date =
    if not (Instance.exists c iname) then raise (No_such_instance iname);

    let l =
      exec c (sprintf "select distinct(project) from entry \
                       where instance = '%s' \
                         and day >= '%s' \
                         and day <= '%s'"
		(quote (string_of_inst_name iname))
		(Date.to_string start_date)
		(Date.to_string end_date)) in
    List.map
      (fun e ->
	 match e with
	     [ d ] ->
	       d
	   | _ ->
	       failwith "Db.Entry.projects: Unexpected return list of SELECT"
      )
      l

  let get c iname date =
    if not (Instance.exists c iname) then
      raise(No_such_instance iname);
    let l =
      exec c (sprintf "select id,row_index,period_start,period_start is null, \
                              period_end,period_end is null, \
                              duration,duration is null,project,description \
                       from entry \
                       where instance = '%s' \
                         and day = '%s'"
		(quote (string_of_inst_name iname))
		(Date.to_string date))
    in
    let l' =
      List.map
	(fun e ->
	   match e with
	       [id;index;period_s;period_s_null;period_e;period_e_null;
		duration;duration_null;project;descr] ->
		 { e_id = Some(int_of_string id);
		   e_instance = iname;
		   e_day = date;
		   e_index = int_of_string index;
		   e_start =
		     if is_bool_true period_s_null then
		       None
		     else
		       Some(Time.from_string period_s);
		   e_end =
		     if is_bool_true period_e_null then
		       None
		     else
		       Some(Time.from_string period_e);
		   e_duration =
		     if is_bool_true duration_null then
		       None
		     else
		       Some(Interval.from_string duration);
		   e_project = project;
		   e_description = descr;
		 }
	     | _  ->
		 failwith "Db.Entry.get: Unexpected return list of SELECT"
	)
	l
    in
    let l'' = List.sort (fun e e' -> compare e.e_index e'.e_index) l' in
    { d_instance = iname;
      d_day = date;
      d_entries = l'';
    }

  let delete c iname date =
    (* Internal function *)
    exec_nr c (sprintf "delete from entry \
                        where instance = '%s' \
                          and day = '%s'"
		 (quote (string_of_inst_name iname))
		 (Date.to_string date)
	      )

  let opt_literal pre post f =
    function
      None -> "null"
    | Some s -> pre ^ f s ^ post

  let insert c l =
    (* Internal function *)
    List.iter
      (fun e ->
	 let new_or_old_id =
	   match e.e_id with
	       Some id -> string_of_int id
	     | None -> expr_next_serial_value "entry" "id"
	 in
	 exec_nr c (sprintf "insert into entry \
                             (id,instance,day,row_index,period_start,period_end, \
                              duration,project,description) \
                             values \
                             (%s,'%s','%s',%d,%s,%s,%s,'%s','%s')"
		      new_or_old_id
		      (quote (string_of_inst_name e.e_instance))
		      (Date.to_string e.e_day)
		      e.e_index
		      (opt_literal
			 (have_time_keyword ^ " '") "'" Time.to_string e.e_start)
		      (opt_literal
			 (have_time_keyword ^ " '") "'" Time.to_string e.e_end)
		      (opt_literal
			 (have_interval_keyword ^ " '")
			 "'" Interval.to_string e.e_duration)
		      (quote e.e_project)
		      (quote e.e_description)
		   );
	 match e.e_id with
	     None ->
	       e.e_id <- Some(get_last_serial_value c "entry" "id")
	   | Some _ -> ()
      )
      l

  let update c day =
    if not (Instance.exists c day.d_instance) then
      raise(No_such_instance day.d_instance);

    (* Check plausibility: *)
    if List.exists (fun e -> e.e_day <> day.d_day) day.d_entries then
      invalid_arg "Db.Entry.update: invalid date found";

    if List.exists (fun e -> e.e_instance <> day.d_instance) day.d_entries then
      invalid_arg "Db.Entry.update: invalid instance found";

    (* Check that every index occurs at most once: *)
    let h = Hashtbl.create 10 in
    List.iter
      (fun e ->
	 if Hashtbl.mem h e.e_index then
	   invalid_arg "Db.Entry.update: index occurs more than once";
	 Hashtbl.add h e.e_index ()
      )
      day.d_entries;

    (* Delete the day, and insert it again: *)
    delete c day.d_instance day.d_day;
    insert c day.d_entries

  let log ?at c (User login) day =
    let at_str =
      match at with
        | None -> "current_timestamp"
        | Some ts -> sprintf "to_timestamp(%s)" (Timestamp.to_epoch_string ts) in
    if day.d_entries = [] then (
      (* record a 'D' change *)
      let new_id = expr_next_serial_value "entry_log" "id" in
      exec_nr c (sprintf
                   "insert into entry_log \
                    (id,instance,day, \
                      change_type,change_time,change_user) \
                    values \
                      (%s,'%s','%s','D',%s,'%s')"
                   new_id
                   (quote (string_of_inst_name day.d_instance))
                   (Date.to_string day.d_day)
                   at_str
                   (quote login)
                )
    ) else
      (* record an 'U' change *)
      List.iter
        (fun e ->
          let new_id = expr_next_serial_value "entry_log" "id" in
          exec_nr c (sprintf
                       "insert into entry_log \
                          (id,instance,day, \
                          change_type,change_time,change_user, \
                          row_index,period_start,period_end, \
                          duration,project,description)
                        values \
                          (%s,'%s','%s','U',%s,'%s', \
                          %d,%s,%s,%s,'%s','%s')"
                       new_id
                       (quote (string_of_inst_name e.e_instance))
                       (Date.to_string e.e_day)
                       at_str
                       (quote login)
                       e.e_index
                       (opt_literal
                          (have_time_keyword ^ " '") "'" Time.to_string e.e_start)
                       (opt_literal
                          (have_time_keyword ^ " '") "'" Time.to_string e.e_end)
                       (opt_literal
                          (have_interval_keyword ^ " '")
                          "'" Interval.to_string e.e_duration)
                       (quote e.e_project)
                       (quote e.e_description)
                    )
        )
        day.d_entries

  let get_change_entries c iname date change_time =
    if not (Instance.exists c iname) then
      raise(No_such_instance iname);
    let l =
      exec c (sprintf "select id,row_index,period_start,period_start is null, \
                              period_end,period_end is null, \
                              duration,duration is null,project,description \
                       from entry_log \
                       where instance = '%s' \
                         and day = '%s' \
                         and change_time >= to_timestamp(%s)
                         and change_time < to_timestamp(%s)"
                (quote (string_of_inst_name iname))
                (Date.to_string date)
                (Timestamp.to_epoch_string change_time)
                (Timestamp.to_epoch_string (Int64.succ change_time))
             ) in
    List.map
      (fun e ->
        match e with
          | [id;index;period_s;period_s_null;period_e;period_e_null;
             duration;duration_null;project;descr] ->
              { e_id = Some(int_of_string id);
                e_instance = iname;
                e_day = date;
                e_index = int_of_string index;
                e_start =
                  if is_bool_true period_s_null then
                    None
                  else
                    Some(Time.from_string period_s);
                e_end =
                  if is_bool_true period_e_null then
                    None
                  else
                    Some(Time.from_string period_e);
                e_duration =
                  if is_bool_true duration_null then
                    None
                  else
                    Some(Interval.from_string duration);
                e_project = project;
                e_description = descr;
              }
          | _  ->
              failwith "Db.Entry.get_change_entries: Unexpected return list of SELECT"
      )
      l
    |> List.sort (fun e e' -> compare e.e_index e'.e_index)

  let remove_dups l =
    let rec recurse old_day l =
      match l with
        | change :: l' ->
            let old_entries =
              if change.mc_change.d_day = old_day.d_day then
                old_day.d_entries
              else
                [] in
            if old_entries = change.mc_change.d_entries then
              recurse change.mc_change l'
            else
              change :: recurse change.mc_change l'
        | [] -> [] in
    recurse
      { d_instance = Instance "";
        d_day = Date.invalid;
        d_entries = []
      }
      l

  let changes c iname start_date end_date =
    if not (Instance.exists c iname) then raise (No_such_instance iname);

    let l =
      exec c (sprintf "select distinct day, extract(epoch from change_time), change_type, change_user from entry_log \
                       where instance = '%s' \
                         and day >= '%s' \
                         and day <= '%s'"
                (quote (string_of_inst_name iname))
		(Date.to_string start_date)
		(Date.to_string end_date)) in
    List.map
      (fun e ->
	 match e with
           | [ d; ts; ct; cu ] ->
               let day = Date.from_string d in
               let mc_timestamp = Db_types.Timestamp.from_epoch_string ts in
               let mc_login = User cu in
               ( match ct with
                   | "D" ->
                       { mc_timestamp;
                         mc_login;
                         mc_change = { d_instance = iname;
                                       d_day = day;
                                       d_entries = []
                                     }
                       }
                   | "U" ->
                       let d_entries =
                         get_change_entries c iname day mc_timestamp in
                       { mc_timestamp;
                         mc_login;
                         mc_change = { d_instance = iname;
                                       d_day = day;
                                       d_entries
                                     }
                       }
                   | _ ->
                       failwith "Db.Entry.changes: Unexpected return list of SELECT"
               )
	   | _ ->
               failwith "Db.Entry.changes: Unexpected return list of SELECT"
      )
      l
    |> List.sort
         (fun c1 c2 ->
           let d = Date.cmp c1.mc_change.d_day c2.mc_change.d_day in
           if d <> 0 then d else
             Int64.compare c1.mc_timestamp c2.mc_timestamp
         )
    |> remove_dups
    |> List.sort
         (fun c1 c2 ->
           let d = Int64.compare c1.mc_timestamp c2.mc_timestamp in
           if d <> 0 then d else
             Date.cmp c1.mc_change.d_day c2.mc_change.d_day
         )
end

module Session = struct
  open Wd_types
  open Connection
  open Printf

  exception Invalid_session_checksum = Wd_dialog.Invalid_session_checksum
  exception Session_not_found = Wd_dialog.Session_not_found


  class db_session_manager db : session_manager_type =
    Wd_dialog.database_session_manager
      ~allocate:(fun () ->
		   (* Delete very old records: *)
  		   exec_nr
		   db
		   (sprintf "delete from wd_session \
                                  where last_used < (current_date - %s)"
		      (Connection.day_interval 7)
		   );
		   (* Compute a new key: *)
		   exec_nr
		     db
		     (sprintf "insert into wd_session \
                                 (id, skey, svalue, schecksum, last_used) \
                                 values (%s, null, '', '', current_date)"
			(expr_next_serial_value "wd_session" "id"));
		   get_last_serial_value db "wd_session" "id")
      ~insert:(fun id key ->
		 exec_nr
		 db
		 (sprintf
		    "update wd_session set skey = '%s' where id = %d" key id);
	      )
      ~update:(fun id key value checksum ->
		 exec_nr
		 db
		 (sprintf "update wd_session \
                           set svalue = '%s', \
                               schecksum = '%s', \
                               last_used = current_date \
                           where skey = '%s'"
		    value
		    checksum
		    key);
		 commit db;
	      )
      ~lookup:(fun id key ->
		 let slice_length = 7000 in
		 (* Retrieve the checksum: *)
		 let checksum =
		   let rows =
		     exec db
		       (sprintf
			  "select schecksum from wd_session where skey = '%s'"
			  (quote key)) in
		   match rows with
		       [ [ v_checksum ] ] -> v_checksum
		     | _                  -> raise Session_not_found
		 in
		 (* Now load the session string *)
		 let slices = ref [] in
		 let last_length = ref 1 in
		                   (* 0 means: end of string reached *)
		 let pos = ref 1 in
		 while !last_length <> 0 do
		   let rows =
		     exec db
		       (sprintf "select substring(svalue from %d for %d) \
                                 from wd_session where skey = '%s'"
			  !pos
			  slice_length
			  (quote key)) in
		   ( match rows with
			 [ [ v ] ] ->
			   slices := v :: !slices;
			   pos := !pos + String.length v;
			   last_length := String.length v
		       | _ ->
			   assert false
		   )
		 done;
		 (* Concatenate the slices and create the session object *)
		 let value =
		   String.concat "" (List.rev !slices) in
		 (value, checksum)
	      )
      ()
end
