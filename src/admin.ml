(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Wd_dialog
open Wd_run_cgi
open Wd_types

open Definitions

class admin db universe name env =
object (self)
  inherit dialog universe name env
  inherit error_behaviour    

  method private check_auth () =
    check_auth db (self # dialog_variable "session")

  method private try_prepare_page() = ()

  method private try_handle() =
    try
      raise(Change_dialog(Filemenu.handle (self :> dialog_type) db self#event))
    with
	Filemenu.Not_competent -> 
	  ( try
	      raise(Change_dialog(Taskmenu.handle (self :> dialog_type) 
				                  self#event))
	    with
		Taskmenu.Not_competent -> 
		  ( match self # event with
			Button "cont-error" ->
			  self # set_next_page "admin-start";
		      | _ ->
			  ()
		  )
	  )
end
;;

Registry.new_admin := 
  fun universe env opt_session ->
    let dlg = universe # create env "admin" in
    dlg # set_variable "session" (Dialog_value opt_session);
    dlg
;;
