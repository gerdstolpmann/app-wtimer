(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Db.Connection
open Db.Types
open Db_types.Types

open Netchannels   (* for in_obj_channel, out_obj_channel *)

(* All export functions write a valid XML document to the out_obj_channel
 * representing the contents of the database [connection], or a subset of
 * that, as follows:
 *
 * - export_users generates a document with the wef:users element as root
 * - export_instances generates a document with the wef:instances element as
 *   root
 * - export_dataset generates a document with the wef:dataset element as root
 *
 * By default, these functions output all users and all instances. The
 * optional arguments restrict the set:
 *
 * - users: Restricts the users who will be mentioned in wef:user and wef:allow
 *   to the passed list
 * - instances: Restricts the instances that will be mentioned in wef:instance
 *   to the passed list
 * - start_date: No entry earlier than this date will be output
 * - end_date: No entry later than this date will be output
 *
 * All export functions generate UTF-8 strings.
 *)

val export_users : 
      ?users:user_name list -> 
      connection -> 
      out_obj_channel -> 
	unit

val export_instances : 
      ?instances: inst_name list ->
      ?start_date: date ->
      ?end_date: date ->
      connection -> 
      out_obj_channel -> 
	unit

val export_instanceLogs :
      ?instances: inst_name list ->
      ?start_date: date ->
      ?end_date: date ->
      connection ->
      out_obj_channel ->
	unit

val export_dataset :
      ?users:user_name list -> 
      ?instances: inst_name list ->
      ?start_date: date ->
      ?end_date: date ->
      connection -> 
      out_obj_channel -> 
	unit


(* The [import] function reads the XML document from the [in_obj_channel],
 * and puts the data into the database [connection]. It is possible that
 * there is a difference between the already existing database records and the
 * newly added records. In this case, a conflict strategy is applied:
 *
 * `Fail: The exception [Conflict] is raised, and the import stops.
 * `Overwrite: Read records overwrite existing records
 * `Only_add: Read records are only accepted when they are new.
 *
 * The smallest units are days, i.e. either a whole day is overwritten or
 * added, or it is left as it is, but it is never only partly changed.
 *)

type conflict_strategy =
  [ `Fail
  | `Overwrite
  | `Only_add
  ]

exception Conflict of string

val import :
      ?onconflict: conflict_strategy ->  (* default: `Fail *)
      connection ->
      in_channel (* in_obj_channel *) ->   (* TODO *)
	unit
