(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

exception Not_competent

let handle dlg event =
  let universe = dlg # universe in
  let env = dlg # environment in
  let session = dlg # dialog_variable "session" in
  match event with
      Button("task-password") ->
	!Registry.new_admin_password universe env session
    | Button("task-accounts") ->
	!Registry.new_admin_accounts universe env session
    | Button("task-access") ->
	!Registry.new_admin_access universe env session
(*
    | Button("task-prefs") ->
	!Registry.new_timetravel universe env session
*)
    | _ ->
	raise Not_competent
;;
