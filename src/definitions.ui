<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
  - <COPYRIGHT>
  - Copyright 2003 Gerd Stolpmann
  -
  - <GPL>
  - This file is part of WTimer.
  -
  - WTimer is free software; you can redistribute it and/or modify
  - it under the terms of the GNU General Public License as published by
  - the Free Software Foundation; either version 2 of the License, or
  - (at your option) any later version.
  -
  - WTimer is distributed in the hope that it will be useful,
  - but WITHOUT ANY WARRANTY; without even the implied warranty of
  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  - GNU General Public License for more details.
  -
  - You should have received a copy of the GNU General Public License
  - along with WDialog; if not, write to the Free Software
  - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  - </>
  -->

<!--  $Id$
   -  ======================================================================
   -
  -->

<!-- This file contains general definitions -->

<ui:template name="start-grid"
             from-caller="main-col html-head html-foot">
  <!-- This template defines the grid for the start page:

       ###############################################
       #                                             #
       # main-col                                    #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       ###############################################
    -->
  <ui:default name="html-head"></ui:default>
  <ui:default name="html-foot"></ui:default>
  <!-- The startpage annunces itself as HTML-4.01, and uses always the
     - CSS stylesheet. However, the page actually does not depend on these
     - features, so older browser display a "useable" page.
    -->
  <t:wd-doctype-html401-strict/>
  <html>
    <head>
      <title>&application-name;</title>
      <meta http-equiv="Content-Script-Type" content="text/javascript"/>
      <link rel="stylesheet" type="text/css"
	    href="&static-url-prefix;definitions.css"/>
      <script type="text/javascript"
	      src="&static-url-prefix;definitions.js"/>
      ${html-head}
    </head>
    <body>
      ${main-col}
      ${html-foot}
    </body>
  </html>
</ui:template>

<!-- ********************************************************************** -->

<ui:template name="basic-grid"
             from-caller="nav-col main-col headline html-head html-foot
                          have_scroll_position grid-style">
  <!-- This template defines the basic grid for the whole application.
       The screen is divided up into three blocks, like

       ###############################################
       #           # headline                        #
       # nav-col   ###################################
       #           #                                 #
       #           # main-col                        #
       #           #                                 #
       #           #                                 #
       #           #                                 #
       #           #                                 #
       #           #                                 #
       #           #                                 #
       ###############################################

       The left column, nav-col, contains mainly navigation links.
       The right column, main-col, contains the current page of the
       application.
       The headline contains the headline.

       This template interprets session.grid (which must already be
       initialized), but you can override this by passing the
       parameter ${grid-style}. See the file session.ui for explanations
       to the grid style.

       If ${have_scroll_position} is "yes", the template enables code
       that saves the scroll position of the main column, and restores
       it in the following page view. There must be a variable
       "scroll_position" in the dialog that uses this template.
       See also definitions.js for the code handling the scroll position.

       ${html-head} is HTML code to include into the HTML header.
       ${html-foot} is HTML code to include at the end of the HTML body.
    -->
  <ui:default name="html-head"></ui:default>
  <ui:default name="html-foot"></ui:default>
  <ui:default name="have_scroll_position">no</ui:default>
  <ui:default name="grid-style"
    ><ui:enumerate variable="session.grid" template="wd-int"/></ui:default>
  <!-- DOCTYPE: For the legacy grid, select HTML-3.2, for the positioned
     - grids select HTML-4.01 (strict)
    -->
  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      <t:wd-doctype-html32/>
    </ui:ifvar>
    <ui:true>
      <t:wd-doctype-html401-strict/>
    </ui:true>
  </ui:cond>
  <!-- document begins: -->
  <html>
    <head>
      <title>&application-name;</title>
      <!-- Set the script language to javascript: -->
      <meta http-equiv="Content-Script-Type" content="text/javascript"/>
      <!-- Include stylesheet only for positioned grids: -->
      <ui:cond>
	<ui:ifvar variable="session.grid" value="legacy" op="contains"/>
	<ui:true>
	  <link rel="stylesheet" type="text/css"
	        href="&static-url-prefix;definitions.css"/>
	</ui:true>
      </ui:cond>
      <!-- Include javascript functions: -->
      <script type="text/javascript"
	      src="&static-url-prefix;definitions.js"/>
      ${html-head}
    </head>
    <!-- Note that this bgcolor (as many other color settings) is only
       - included for legacy browsers. Colors are overriden by CSS rules.
      -->
    <body onload="global_load_handler();" bgcolor="#FFFFE0">
      <ui:form onsubmit="return global_submit_handler();">
        <!-- If scroll position management is enabled, define a hidden form
           - field that stores the scroll position.
          -->
	<ui:if value1="$have_scroll_position" value2="yes">
	  <t:basic-grid-scroll/>
	</ui:if>
        <!-- Position the three display boxes, i.e. define the "grid": -->
	<ui:cond>
	  <ui:if value1="${grid-style}" value2="legacy">
	    <!-- Legacy: Tabled-based grid: -->
	    <table cellspacing="0" cellpadding="0" width="100%">
	      <tr>
		<td valign="top" width="150" bgcolor="#CDBA96">
		  <table cellspacing="0" cellpadding="15" width="150">
		    <tr><td width="150" align="center"
			><font size="3" color="black">${nav-col}</font
			  ></td></tr>
		  </table>
		</td>
		<td valign="top" width="100%">
		  <table cellspacing="0" cellpadding="15" width="100%">
		    <tr>
		      <td bgcolor="#ff8c00">
			<font size="5" color="white">${headline}</font>
		      </td>
		    </tr>
		    <tr>
		      <td align="center">
			<font size="3" color="black">${main-col}</font>
		      </td>
		    </tr>
		  </table>
		</td>
	      </tr>
	    </table>
	  </ui:if>
	  <ui:true>
	    <!-- Modern: Positioned grids: -->
	    <div id="basic-grid-screen" class="grid-screen-${grid-style}">
	      <ui:if value1="${grid-style}" value2="960">
		<basefont size="2"/>  <!-- force small fonts -->
	      </ui:if>
	      <div id="basic-grid-headline" class="grid-head-${grid-style}">
		<div class="grid-inner-head">${headline}</div>
	      </div>
	      <div id="basic-grid-sep1">&nbsp;</div>
	      <div id="basic-grid-nav-col" class="grid-nav-col-${grid-style}"
		style="background-image: url(&static-url-prefix;menubg.jpeg);">
		<div id="basic-grid-inner-nav-col">${nav-col}</div>
	      </div>
	      <div id="basic-grid-sep2">&nbsp;</div>
	      <div id="basic-grid-sep3">&nbsp;</div>
	      <!-- Detail: tabindex="1" is non-standard but interpreted by
	         - MSIE. It enables that this DIV can get the focus.
	        -->
	      <div id="basic-grid-main-col"
		   class="grid-main-col-${grid-style}"
		   tabindex="1"
	      >
		<div class="grid-inner-main-col">${main-col}</div>
	      </div>
	    </div>
	  </ui:true>
	</ui:cond>
      </ui:form>
      ${html-foot}
    </body>
  </html>
</ui:template>


<ui:template name="basic-grid-scroll">
  <input type="hidden" name="var_scroll_position"
         value="$[scroll_position]"/>
  <!-- This hidden field is used to remember the Y scroll position
     - of the DIV element basic-grid-main-col (if supported by
     - browser). It is filled by the global_submit_handler,
     - and interpreted by the global_load_handler. Note that a
     - string variable "scroll_position" is needed in the dialog
     - to enable this feature.
    -->
  <ui:cond>
    <ui:ifvar variable="session.grid" value="fixed" op="contains">
      <input type="hidden" name="scrolled_element" value="frame"/>
    </ui:ifvar>
    <ui:true>
      <input type="hidden" name="scrolled_element" value="basic-grid-main-col"/>
    </ui:true>
  </ui:cond>
  <!-- This hidden field specifies which HTML element is scrolled.
     - This is the frame/window for "fixed", and the main column
     - for the other grids.
    -->

</ui:template>

<!-- ********************************************************************** -->

<ui:template name="popup-grid"
             from-caller="main-col headline html-head">
  <!-- This template defines the grid for popup windows.

       ###############################################
       # headline                                    #
       ###############################################
       #                                             #
       # main-col                                    #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       #                                             #
       ###############################################

       The main column, main-col, contains the current page of the
       popup window.
       The headline contains the headline.
    -->
  <ui:default name="html-head"></ui:default>
  <!-- DOCTYPE: For the legacy grid, select HTML-3.2, for the positioned
     - grids select HTML-4.01 (strict)
    -->
  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      <t:wd-doctype-html32/>
    </ui:ifvar>
    <ui:true>
      <t:wd-doctype-html401-strict/>
    </ui:true>
  </ui:cond>
  <!-- document begins -->
  <html>
    <head>
      <title>WTimer Web Application</title>
      <!-- Set the script language to javascript: -->
      <meta http-equiv="Content-Script-Type" content="text/javascript"/>
      <!-- Include stylesheet only for positioned grids: -->
      <ui:cond>
	<ui:ifvar variable="session.grid" value="legacy" op="contains"/>
	<ui:true>
	  <link rel="stylesheet" type="text/css"
	        href="&static-url-prefix;definitions.css"/>
	</ui:true>
      </ui:cond>
      <!-- Include javascript functions: -->
      <script type="text/javascript"
	      src="&static-url-prefix;definitions.js"/>
      ${html-head}
    </head>
    <!-- Note that this bgcolor (as many other color settings) is only
       - included for legacy browsers. Colors are overriden by CSS rules.
      -->
    <body bgcolor="#FFFFE0">
      <ui:form>
	<ui:cond>
	  <ui:ifvar variable="session.grid" value="legacy" op="contains">
	    <!-- Legacy grid -->
	    <table cellspacing="0" cellpadding="5" width="100%">
	      <tr>
		<td bgcolor="#ff8c00">
		  <font size="5" color="white">${headline}</font>
		</td>
	      </tr>
	      <tr>
		<td><font size="3" color="black">${main-col}</font></td>
	      </tr>
	    </table>
	  </ui:ifvar>
	  <ui:true>
	    <!-- Positioned grid -->
	    <div id="popup-grid-screen">
	      <div id="popup-grid-headline">
		<div class="popup-grid-inner-headline">${headline}</div>
	      </div>
	      <div id="popup-grid-main-col">
		<div class="popup-grid-inner-main-col">${main-col}</div>
	      </div>
	    </div>
	  </ui:true>
	</ui:cond>
      </ui:form>
    </body>
  </html>
</ui:template>

<!-- ********************************************************************** -->

<ui:template name="head-info-box">
  <!-- The head-info-box is normally displayed at the right edge of the
     - headline, and includes information about the current user and the
     - current date. These data are taken from the session variable, which
     - must exist.
    -->

  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      <!-- legacy grid -->
	<table cellspacing="0" border="1" align="right">
	  <tr>
	    <td>Logged in as: $[session.login-user]</td>
	    <td>Selected sheet: $[session.current-sheet]</td>
	    <td>Month:
	      $[session.current-date/year], $[session.current-date/month-en]</td>
	  </tr>
	</table>
    </ui:ifvar>
    <ui:true>
      <!-- positioned grid -->
      <div id="head-info-box">
	<table cellspacing="0">
	  <tr>
	    <td>Logged in as: $[session.login-user]</td>
	    <td>Selected sheet: $[session.current-sheet]</td>
	    <td>Month:
	      $[session.current-date/year], $[session.current-date/month-en]</td>
	  </tr>
	</table>
      </div>
    </ui:true>
  </ui:cond>
</ui:template>

<!-- ********************************************************************** -->

<ui:template name="head-modified-flag">
  <!-- The "is modified" flag appears if the user has done modifications
     - of the data, but has not yet saved them. Currently, this flag is
     - visualized by the text "(modified)" which is normally appended
     - to the headline.
     - There are three modes of operation:
     - (1) legacy mode: the flag is not supported at all
     - (2) static mode: the flag is controled by the variable "modified",
     -     which is either "yes" or "no". Depending on the value the CSS
     -     class of the flag is selected, and the "visibility" attribute is
     -     used to show or hide the flag.
     -     It is required that the variable "modified" is set to the
     -     right value after every click.
     - (3) dynamic mode: by DOM methods the style of the flag is changed
     -     dynamically. The javascript function set_modified_flag makes
     -     the flag appear on the screen. It should be called by onchange
     -     handlers. (Defined in definitions.js)
    -->
  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      &nbsp;  <!-- legacy grid: no modified flag! -->
    </ui:ifvar>
    <ui:true> <!-- positioned grid -->
      <span id="modified" class="head-mod-$[modified]">(modified)</span>
    </ui:true>
  </ui:cond>
</ui:template>

<!-- ********************************************************************** -->

<!-- The nav-col contains the menu, which looks like:
   - <q:nav-section head="Section1">
   -   <q:nav-entry title="Long explanation for button1">
   -     <q:applink label="Label button1" name="button1" .../>
   -   </q:nav-entry>
   -   ... more nav-entry instances ...
   - </q:nav-section>
   - ... more nav-section menus ...
  -->

<ui:template name="nav-section" from-caller="head body">
  <!-- A section of the navigation bar, i.e. a menu. There is a headline
     - and a body, the latter containing buttons.
    -->
  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      <!-- legacy grid: use bgcolor to emphasize the section heads -->
      <table cellspacing="0" cellpadding="0" width="100%">
	<tr>
	  <td bgcolor="#8B7E66" align="left" width="100%">
	    <table cellspacing="0" cellpadding="3" width="100%">
	      <tr><td>
		  <font size="4" color="white"><b>$head</b></font>
		</td></tr></table></td>
	</tr>
	$body
      </table>
      <t:legacy-vspacer size="5"/>
    </ui:ifvar>
    <ui:true>
      <!-- positioned grid: a CSS solution -->
      <div class="nav-section-head">$head</div>
      <div class="nav-section-body">$body</div>
    </ui:true>
  </ui:cond>
</ui:template>

<ui:template name="nav-entry" from-caller="title body">
  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      <!-- legacy grid: Ignore title -->
      <tr>
	<td align="center">
	  <font size="2" color="black">$body</font></td>
      </tr>
    </ui:ifvar>
    <ui:true>
      <!-- positioned grid: a CSS solution -->
      <div class="nav-section-link"
           title="$title">$body</div>
    </ui:true>
  </ui:cond>
</ui:template>

<!-- ********************************************************************** -->

<ui:template name="vspacer" from-caller="size">
  <!-- Generates a block-level box with the passed ${size} (pixels). -->
  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      <t:legacy-vspacer size="$size"/>
    </ui:ifvar>
    <ui:true>
      <div style="height: ${size}px"></div>
    </ui:true>
  </ui:cond>
</ui:template>


<ui:template name="legacy-vspacer" from-caller="size">
  <!-- Generates a block-level box with the passed ${size} (pixels),
     - but only when the legacy grid is selected. This can be useful
     - to add space where normally CSS definitions define spacing.
    -->
  <ui:ifvar variable="session.grid" value="legacy" op="contains">
    <div><table height="$size"><tr><td>&nbsp;</td></tr></table></div>
  </ui:ifvar>
</ui:template>

<!-- ********************************************************************** -->

<!-- applink: Creates a button that looks like a hyperlink. It has the
     following parameters:
     - name: The name of the button
     - label: The visible label on the button
     - goto (optional): The page to which the button is connected
     - accesskey (optional): A key that is bound to the button
     - disabled (optional, "yes" or "no"): Whether the button is
       disabled (has no effect)
     - emphasis (optional, "yes" or "no"): Whether to emphasize this
       link

     Note: there are two implementation of applink. The advanced implementation
     is only used if the dynamic grid is enabled, and the normal implementation
     in the other cases. The variable session.button-impl selects the
     implementation.

     The advanced applinks use the HTML BUTTON element which is new in
     HTML-4, and many CSS tricks to implement some special effects. Currently
     only Gecko-based browsers work. Note that BUTTON elements are called
     ui:richbutton in WDialog, and so you have to set session.button-impl
     to the value "richbutton" to enable this implementation.

     The normal applinks use the HTML INPUT[type=submit] elements, and are
     a robust base implementation. Note that these button elements are called
     ui:button in WDialog, and so you have to set session.button-impl
     to the value "button" to enable this implementation.
  -->

<ui:template name="applink"
             from-caller="accesskey label name goto disabled emphasis">
  <ui:default name="goto"></ui:default>  <!-- default value: empty string -->
  <ui:default name="accesskey"></ui:default>
  <ui:default name="disabled">no</ui:default>
  <ui:default name="emphasis">no</ui:default>
  <!-- Pass these parameters to subsequent templates by context: -->
  <ui:context>
    <ui:param name="accesskey">$accesskey</ui:param>
    <ui:param name="label">$label</ui:param>
    <ui:param name="name">$name</ui:param>
    <ui:param name="goto">$goto</ui:param>
    <ui:param name="emphasis">$emphasis</ui:param>
    <!-- Depending on the passed parameter, call one of the specialized
         templates below.
      -->
    <ui:cond>
      <ui:if value1="$disabled" value2="yes">
	<ui:use template="applink-$[session.button-impl]-disabled"/>
      </ui:if>
      <ui:if value1="$accesskey/$goto" value2="/">
	<ui:use template="applink-$[session.button-impl]-normal"/>
      </ui:if>
      <ui:if value1="$accesskey/$goto" value2="^/" op="match">
	<ui:use template="applink-$[session.button-impl]-goto"/>
      </ui:if>
      <ui:if value1="$accesskey/$goto" value2="/$$" op="match">
	<ui:use template="applink-$[session.button-impl]-accesskey"/>
      </ui:if>
      <ui:true>
	<ui:use template="applink-$[session.button-impl]-accesskey-goto"/>
      </ui:true>
    </ui:cond>
  </ui:context>
</ui:template>

<!-- The implementation of applink basing on BUTTON/ui:richbutton:
   - This should only be used for browsers with good HTML4/CSS2 compliance.
   - It has a number of "special effects": The layout of the buttons
   - is completely redefined. The background has a "hover" effect.
   - The accesskey appears when the pointer is over the button.
  -->

<ui:template name="applink-richbutton-disabled" from-context="label">
  <!-- Internally used by "applink" -->
  <button name="_unused_" disabled="disabled">
    <div>
      <table cellspacing="0">
	<tr><td class="applink-disabled"><ui:special>&amp;otimes;</ui:special></td>
	    <td class="applink-label-disabled">($label)</td>
	</tr>
      </table>
    </div>
  </button>
</ui:template>


<ui:template name="applink-richbutton-normal"
             from-context="label name emphasis">
  <!-- Internally used by "applink" -->
  <ui:richbutton name="$name" cgi="keep">
    <div>
      <table cellspacing="0">
	<tr><td class="applink-em-$emphasis"><ui:special
		>&amp;otimes;</ui:special></td>
	    <td class="applink-label-em-$emphasis">$label</td>
	</tr>
      </table>
    </div>
  </ui:richbutton>
</ui:template>


<ui:template name="applink-richbutton-accesskey"
             from-context="accesskey label name emphasis">
  <!-- Internally used by "applink" -->
  <ui:richbutton name="$name" accesskey="$accesskey" cgi="keep">
    <div>
      <span class="accesskey">[ALT-$accesskey]</span>
      <table cellspacing="0">
	<tr>
	  <td class="applink-em-$emphasis"><ui:special
		>&amp;otimes;</ui:special></td>
	  <td class="applink-label-em-$emphasis">$label</td>
	</tr>
      </table>
    </div>
  </ui:richbutton>
</ui:template>


<ui:template name="applink-richbutton-goto"
             from-context="label name goto emphasis">
  <!-- Internally used by "applink" -->
  <ui:richbutton name="$name" goto="$goto" cgi="keep">
    <div>
      <table cellspacing="0">
	<tr><td class="applink-em-$emphasis"><ui:special
		>&amp;otimes;</ui:special></td>
	    <td class="applink-label-em-$emphasis">$label</td>
	</tr>
      </table>
    </div>
  </ui:richbutton>
</ui:template>


<ui:template name="applink-richbutton-accesskey-goto"
             from-context="accesskey label name goto emphasis">
  <!-- Internally used by "applink" -->
  <ui:richbutton name="$name" accesskey="$accesskey" goto="$goto" cgi="keep">
    <div>
      <span class="accesskey">[ALT-$accesskey]</span>
      <table cellspacing="0">
	<tr><td class="applink-em-$emphasis"><ui:special
		>&amp;otimes;</ui:special></td>
	    <td class="applink-label-em-$emphasis">$label</td>
	</tr>
      </table>
    </div>
  </ui:richbutton>
</ui:template>

<!-- The implementation of applink for INPUT TYPE=button / ui:button
   - The buttons are rendered as normal buttons without special effects.
  -->

<ui:template name="applink-button-disabled" from-context="label">
  <!-- Internally used by "applink" -->
  <div class="applink">
    <input type="button" name="_unused_" disabled="disabled" value="$label"
           class="applink-label-disabled"/>
  </div>
</ui:template>


<ui:template name="applink-button-normal"
             from-context="label name emphasis">
  <!-- Internally used by "applink" -->
  <div class="applink">
    <ui:button name="$name" label="$label" border="0" cgi="keep"
               class="applink-label-em-$emphasis"/>
  </div>
</ui:template>


<ui:template name="applink-button-accesskey"
             from-context="accesskey label name emphasis">
  <!-- Internally used by "applink" -->
  <div class="applink">
    <ui:button name="$name" label="$label" accesskey="$accesskey" border="0"
               cgi="keep" class="applink-label-em-$emphasis"/>
  </div>
</ui:template>


<ui:template name="applink-button-goto"
             from-context="label name goto emphasis">
  <!-- Internally used by "applink" -->
  <div class="applink">
    <ui:button name="$name" label="$label" goto="$goto" border="0" cgi="keep"
               class="applink-label-em-$emphasis"/>
  </div>
</ui:template>


<ui:template name="applink-button-accesskey-goto"
             from-context="accesskey label name goto emphasis">
  <!-- Internally used by "applink" -->
  <div class="applink">
    <ui:button name="$name" label="$label" goto="$goto" accesskey="$accesskey"
                cgi="keep" border="0" class="applink-label-em-$emphasis"/>
  </div>
</ui:template>

<!-- *** -->

<ui:template name="applink-appendix" from-caller="body">
  <div style="padding-left: 3px;">
    <table cellspacing="0">
      <tr><td class="applink-em-no">&nbsp;</td>
	  <td class="applink-label-em-no">$body</td>
      </tr>
    </table>
  </div>
</ui:template>

<!-- ********************************************************************** -->

<!-- Messages -->

<ui:template name="message" from-caller="int ext body">
  <!-- int: the name of the message (if from a catalog). The prefix of the
         name determines the category of the message:
         error-*: Error
         warning-*: Warning
         confirm-*: Confirm
         All other: Message
     - ext or body: The text of the message
  -->
  <ui:default name="int"></ui:default>
  <ui:default name="ext"></ui:default>
  <ui:default name="body"></ui:default>
  <ui:cond>
    <ui:ifvar variable="session.grid" value="legacy" op="contains">
      <table width="100%" border="2">
	<tr>
	  <td align="left" valign="baseline"><b>
	    <ui:cond>
	      <ui:if value1="$int" value2="^error-" op="match">Error</ui:if>
	      <ui:if value1="$int" value2="^warning-" op="match">Warning</ui:if>
	      <ui:if value1="$int" value2="^confirm-" op="match">Confirm</ui:if>
	      <ui:true>Message</ui:true>
	    </ui:cond>
          </b></td>
	  <td align="left" valign="baseline">$ext$body</td>
	</tr>
      </table>
      <t:legacy-vspacer size="10"/>
    </ui:ifvar>
    <ui:true>
      <div class="message-box">
	<div class="message-head-box">
	  <div class="message-head">
	    <ui:cond>
	      <ui:if value1="$int" value2="^error-" op="match">Error</ui:if>
	      <ui:if value1="$int" value2="^warning-" op="match">Warning</ui:if>
	      <ui:if value1="$int" value2="^confirm-" op="match">Confirm</ui:if>
	      <ui:true>Message</ui:true>
	    </ui:cond>
	  </div>
	</div>
	<div class="message">$ext$body</div>
      </div>
    </ui:true>
  </ui:cond>
</ui:template>

<!-- ********************************************************************** -->

<!-- Error pages -->

<ui:template name="error" from-caller="errid">
  <!-- errid: a token identifying the error. Catalog:
    -  permission-denied
    -  session-not-found
    -  invalid-session-checksum
    -  no-such-user
    -  user-already-exists
    -  no-such-instance
    -  instance-already-exists
    -  internal-error
    -->
  <ui:use template="error-$errid"/>
</ui:template>


<ui:template name="error-permission-denied">
  <q:message int="error-page">
    <div><b>Permission denied</b></div>
    <div>
      You are not allowed to do this action because of missing
      rights.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-session-not-found">
  <q:message int="error-page">
    <div><b>Session not found</b></div>
    <div>
      Your current session has timed out. Please log in again.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-invalid-session-checksum">
  <q:message int="error-page">
    <div><b>Invalid session checksum</b></div>
    <div>
      The synchronization between your browser and the application
      server is lost. This can happen when you are using the history
      function of your browser ("Back", "Go"). Please avoid doing
      so. You can only continue by logging in again.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-no-such-user">
  <q:message int="error-page">
    <div><b>No such user</b></div>
    <div>
      One of the user accounts has been deleted, and your action
      cannot be carried out because of this.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-user-already-exists">
  <q:message int="error-page">
    <div><b>User already exists</b></div>
    <div>
      One of the user accounts has already been created in the
      meantime, and your action cannot be carried out because
      of this.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-no-such-instance">
  <q:message int="error-page">
    <div><b>No such instance</b></div>
    <div>
      One of the sheets has been deleted, and your action
      cannot be carried out because of this.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-instance-already-exists">
  <q:message int="error-page">
    <div><b>Instance already exists</b></div>
    <div>
      One of the sheets has already been created in the
      meantime, and your action cannot be carried out because
      of this.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-login-not-permitted">
  <q:message int="error-page">
    <div><b>You are not permitted to login</b></div>
    <div>
      Reasons for this may be that you are not a registered
      user, or that your user account is disabled.
    </div>
  </q:message>
</ui:template>

<ui:template name="error-internal-error">
  <q:message int="error-page">
    <div><b>Internal error</b></div>
    <div>
      An error occurred while processing your request. Details can
      be found in the error log of the software. Please inform
      your administrator about the failure.
    </div>
  </q:message>
</ui:template>

<!-- ******************* For Emacs: ********************* -->
<!--
Local Variables:
mode: xml
sgml-parent-document: ("main.ui" "ui:application" "ui:template")
End:
-->
