<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
  - <COPYRIGHT>
  - Copyright 2003 Gerd Stolpmann
  -
  - <GPL>
  - This file is part of WTimer.
  -
  - WTimer is free software; you can redistribute it and/or modify
  - it under the terms of the GNU General Public License as published by
  - the Free Software Foundation; either version 2 of the License, or
  - (at your option) any later version.
  -
  - WTimer is distributed in the hope that it will be useful,
  - but WITHOUT ANY WARRANTY; without even the implied warranty of
  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  - GNU General Public License for more details.
  -
  - You should have received a copy of the GNU General Public License
  - along with WDialog; if not, write to the Free Software
  - Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
  - </>
  -->

<!--  $Id$
   -  ======================================================================
   -
  -->

<!-- This file defines the dialog "timetravel". -->

<ui:dialog name="timetravel" start-page="calendary">
  <ui:variable name="session" type="dialog"/>
    <!-- The session pseudo dialog, see session.ui -->

  <ui:variable name="year">
    <!--&timetravel-year;-->        <!-- testdata -->
  </ui:variable>

  <ui:variable name="entered_year"/>

  <ui:enumeration name="message">
    <ui:enum internal="error-bad-year"
             external="Please enter a number between 1902 and 2037!"/>
  </ui:enumeration>

  <ui:variable name="message" type="message">
    <!--&timetravel-message;-->     <!-- testdata -->
  </ui:variable>

  <ui:page name="calendary">
    <t:basic-grid>
      <p:nav-col>
	<t:timetravel-nav/>
      </p:nav-col>
      <p:headline>
	Time Travel
	<t:head-info-box/>
      </p:headline>
      <p:main-col>
	<div><a href="#help">Help</a><br/>&nbsp;</div>
	<ui:enumerate variable="message" template="message"/>
	<div class="cal-head">
	  Year: <ui:text variable="entered_year" maxlength="4" size="4"
	                 cgi="keep"
	                 onchange="check_year_string(this)"/>
	        <ui:if value1="$[session.js-enabled]" value2="yes">
   	          <input type="button" value="+" class="cal-head-pm"
	                 onclick="inc_year(document.uiform.var_entered_year)"/>
	          <input type="button" value="-"  class="cal-head-pm"
	                 onclick="dec_year(document.uiform.var_entered_year)"/>
                </ui:if>
	        <ui:button label="Update" name="update"/>
	</div>

	<div class="cal-body">
	  <t:calendary-year year="$[year]"/>
	</div>

	<div class="help">
	  <a name="help"/>
	  <div>This dialog allows you to open a certain month of the current
	    sheet. Enter the year, press "Update", and click on the
	    calendar page of the month (or the button below). Use the
	    "+" and "-" buttons (Javascript needed) to go to the next
	    or the previous years, respectively.</div>

	  <div>Note that clicking on the calendar page does not work
	    for (current) Internet Explorers due to incompatibilities with web
	    standards.</div>
	</div>

      </p:main-col>
    </t:basic-grid>
  </ui:page>


  <!-- ERROR HANDLING -->

  <!-- See also ocaml class error_behaviour that uses the following
       definitions
    -->

  <ui:variable name="runtime-error"/>

  <ui:page name="runtime-error">
    <t:basic-grid>
      <p:nav-col>
	<!-- empty nav-col: this is a modal dialog -->
      </p:nav-col>
      <p:headline>
	Time Travel
	<t:head-info-box/>
      </p:headline>
      <p:main-col>
	<t:error errid="$[runtime-error]"/>
	<ui:button name="cont-error" label="Continue"/>
      </p:main-col>
    </t:basic-grid>
  </ui:page>

</ui:dialog>


<ui:template name="calendary-year" from-caller="year">
  <!-- Displays the whole year by a 4x3 table of months -->
  <table border="0" class="cal" cellpadding="10">
    <tr>
      <td class="cal"><t:calendary-month date="$year-01-01"/></td>
      <td class="cal"><t:calendary-month date="$year-02-01"/></td>
      <td class="cal"><t:calendary-month date="$year-03-01"/></td>
    </tr>
    <tr>
      <td class="cal"><t:calendary-month date="$year-04-01"/></td>
      <td class="cal"><t:calendary-month date="$year-05-01"/></td>
      <td class="cal"><t:calendary-month date="$year-06-01"/></td>
    </tr>
    <tr>
      <td class="cal"><t:calendary-month date="$year-07-01"/></td>
      <td class="cal"><t:calendary-month date="$year-08-01"/></td>
      <td class="cal"><t:calendary-month date="$year-09-01"/></td>
    </tr>
    <tr>
      <td class="cal"><t:calendary-month date="$year-10-01"/></td>
      <td class="cal"><t:calendary-month date="$year-11-01"/></td>
      <td class="cal"><t:calendary-month date="$year-12-01"/></td>
    </tr>
  </table>
</ui:template>


<ui:template name="calendary-month" from-caller="date">
  <!-- Displays a single month.
     - date: Must be the first day of the month to display
    -->
  <ui:context>
    <!-- Put $date into context so subsequent templates can access it -->
    <ui:param name="date">$date</ui:param>
    <!-- The whole month is a button: -->
    <ui:richbutton
	name="calendary-month"
	index="${date/year}-${date/month}-01"
	>
      <!-- Generate the table whose columns are weekdays. The table has
         - a fixed layout of 7 columns and 6 rows. This results in 42
         - positions which are numbered from 0 to 41. The last five positions
         - can never be filled, so these get the ordinal number 37.
         - The template calendary-day either puts the day number into
         - the table cells, or leaves them empty.
        -->
      <table border="0" class="cal-month">
	<tr>
	  <td colspan="7" align="center">${date/month-en} ${date/year}</td>
	</tr>
	<tr>
	  <th class="cal-month">Sun</th>
	  <th class="cal-month">Mon</th>
	  <th class="cal-month">Tue</th>
	  <th class="cal-month">Wed</th>
	  <th class="cal-month">Thu</th>
	  <th class="cal-month">Fri</th>
	  <th class="cal-month">Sat</th>
	</tr>
	<tr>
	  <td class="cal-month"><t:calendary-day arg="$date 0"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 1"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 2"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 3"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 4"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 5"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 6"/></td>
	</tr>
	<tr>
	  <td class="cal-month"><t:calendary-day arg="$date 7"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 8"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 9"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 10"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 11"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 12"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 13"/></td>
	</tr>
	<tr>
	  <td class="cal-month"><t:calendary-day arg="$date 14"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 15"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 16"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 17"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 18"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 19"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 20"/></td>
	</tr>
	<tr>
	  <td class="cal-month"><t:calendary-day arg="$date 21"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 22"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 23"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 24"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 25"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 26"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 27"/></td>
	</tr>
	<tr>
	  <td class="cal-month"><t:calendary-day arg="$date 28"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 29"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 30"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 31"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 32"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 33"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 34"/></td>
	</tr>
	<tr>
	  <td class="cal-month"><t:calendary-day arg="$date 35"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 36"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 37"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 37"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 37"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 37"/></td>
	  <td class="cal-month"><t:calendary-day arg="$date 37"/></td>
	</tr>
      </table>
    </ui:richbutton>
    <!-- Legacy browsers cannot interpret the BUTTON element. So add
       - a normal button for them, too.
      -->
    <ui:cond>
      <ui:ifvar variable="session.grid" value="dynamic" op="contains"/>
      <ui:true>
	<center>
	  <ui:button name="calendary-month"
		     index="${date/year}-${date/month}-01"
		     label="Go to ${date/month-en}"/>
	</center>
	&nbsp;<br/>
      </ui:true>
    </ui:cond>
  </ui:context>
</ui:template>


<ui:template name="calendary-day" from-context="date" from-caller="arg">
  <!-- date: the month represented by the first day of the month
     - arg: A string "YYYY-MM-01 N" where YYYY is the year, MM the month,
     -    and N the ordinal number of the cell
     -
     - cal-day is an output encoding that maps arg type strings to
     - the day of the month, or the empty string. See main.ml for its
     - definition.
    -->
  <ui:cond>
    <ui:if value1="${arg/cal-day}" value2="" op="ne">
      ${arg/cal-day}
    </ui:if>
    <ui:true>&nbsp;</ui:true>
  </ui:cond>
</ui:template>

<!-- ********************************************************************** -->

<!-- timetravel-nav: The navigation bar for timetravelling -->

<ui:template name="timetravel-nav">
  <t:file-menu current="file-time-travel"/>
</ui:template>


<!-- ******************* For Emacs: ********************* -->
<!--
Local Variables:
mode: xml
sgml-parent-document: ("main.ui" "ui:application" "ui:dialog")
End:
-->
