(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Db.Connection
open Db.Types
open Db_types.Types
open Db_types

open Pxp_types
open Pxp_tree_parser
open Pxp_reader
open Pxp_document

let wef_namespace = "http://www.ocaml-programming.de/formal/wef-namespace" ;;

let wef_public_id = "-//ocaml-programming.de//DTD WEF 1//EN";;

let wef_system_id = "http://www.ocaml-programming.de/formal/wef-dtd-1";;


let indent = [| ""; "  "; "    "; "      "; "        "; "          " |] ;;

let out_indent out level =
  out # output_string (indent.(level))
;;


let unsafe_chars_xml =
  "<>\"'&\r\n\t";;

let encode =
  Netencoding.Html.encode 
    ~in_enc:Const.internal_charset
    ~out_enc:`Enc_utf8
    ~prefer_name:false
    ~unsafe_chars:unsafe_chars_xml
    ()
;;


let encode_bool =
  function
      false -> "no"
    | true  -> "yes"
;;


let output_decl root_name out =
  out # output_string "<?xml version='1.0' encoding='UTF-8'?>\n";
  out # output_string ("<!DOCTYPE " ^ root_name ^ 
		       " PUBLIC '" ^ wef_public_id ^ "' '" ^ wef_system_id ^ 
		       "'>\n\n")
;;


let output_users ?(level = 0) ?users db out =
  let user_list =
    match users with
	None -> Db.User.list db
      | Some l -> List.map (Db.User.get db) l
  in

  out_indent out level;
  out # output_string "<wef:users";
  if level = 0 then
    out # output_string (" xmlns:wef='" ^ wef_namespace ^ "'>\n")
  else
    out # output_string ">\n";

  List.iter
    (fun user ->
       out_indent out (level+1);
       out # output_string "<wef:user\n";
       ( match user.u_name with
	     User n ->
	       out_indent out (level+2);
	       out # output_string ("name='" ^ encode n ^ "'\n");
       );
       out_indent out (level+2);
       out # output_string ("description='" ^ encode user.u_description ^ "'\n");
       ( match user.u_password with
	     None -> ()
	   | Some pw ->
	       out_indent out (level+2);
	       out # output_string ("password='" ^ encode pw ^ "'\n");
       );
       if user.u_email <> "" then (
         out_indent out (level+2);
         out # output_string ("email='" ^ encode user.u_email ^ "'\n");
       );
       if user.u_managers <> [] then (
         out_indent out (level+2);
         let s =
           user.u_managers
           |> List.map string_of_user_name
           |> String.concat "," in
         out # output_string ("managers='" ^ encode s ^ "'\n");
       );
       out_indent out (level+2);
       out # output_string ("login='" ^ encode_bool user.u_login ^ "'\n");
       out_indent out (level+2);
       out # output_string ("admin='" ^ encode_bool user.u_admin ^ "'/>\n");
    )
    user_list;

  out_indent out level;
  out # output_string "</wef:users>\n"
;;


let very_early = Date.from_string "1900-01-01";;  (* TODO *)
let very_late = Date.from_string "2200-12-31";;   (* TODO *)

let iter_months start_date end_date f =
  let start_rec = Date.access start_date in
  let end_rec = Date.access end_date in
  for year = start_rec.year to end_rec.year do
    let smonth =
      if year = start_rec.year then start_rec.month else 1 in
    let emonth =
      if year = end_rec.year then end_rec.month else 12 in
    for month = smonth to emonth do
      f year month
    done
  done

let output_instances ?(level = 0) ?users ?instances 
                     ?(start_date = very_early) 
		     ?(end_date = very_late) db out =
  let inst_list =
    match instances with
	None -> Db.Instance.list db
      | Some l -> List.map (Db.Instance.get db) l
  in

  let user_name_list =
    match users with
	None -> 
	  List.map (fun user -> user.u_name) (Db.User.list db)
      | Some l -> 
	  List.iter 
	    (fun u -> ignore(Db.User.get db u)) 
	    l;                                    (* assert that users exist *)
	  l
  in

  out_indent out level;
  out # output_string "<wef:instances";
  if level = 0 then
    out # output_string (" xmlns:wef='" ^ wef_namespace ^ "'>\n")
  else
    out # output_string ">\n";

  List.iter
    (fun inst ->
       ( match inst.i_name with
	     Instance n ->
	       out_indent out (level+1);
	       out # output_string ("<wef:instance\n");
	       out_indent out (level+2);
	       out # output_string ("name='" ^ encode n ^ "'\n");
	       out_indent out (level+2);
	       out # output_string ("description='" ^ encode inst.i_description
				    ^ "'>\n");
       );
       
       let perms = Db.Permission.get db inst.i_name in
       if perms.p_set <> [] then begin
	 out_indent out (level+2);
	 out # output_string "<wef:permission>\n";
	 List.iter
	   (fun (User u, p) ->
	      if List.mem (User u) user_name_list then begin
		out_indent out (level+3);
		out # output_string ("<wef:allow user='" ^ 
				     encode u ^ 
				     "' right='" ^ 
				     ( match p with
					   `Write -> "write"
					 | `Read -> "read"
					 | `Owner -> "owner"
				     ) ^ "'/>\n");
	      end
	   )
	   perms.p_set;
	 out_indent out (level+2);
	 out # output_string "</wef:permission>\n";
       end;

       let dates = Db.Entry.list db inst.i_name start_date end_date in
       List.iter
	 (fun date ->
	    out_indent out (level+2);
	    out # output_string ("<wef:day date='" ^ Date.to_string date ^ 
				 "'>\n");
	    
	    let day = Db.Entry.get db inst.i_name date in
	    List.iter
	      (fun entry ->
		 out_indent out (level+3);
		 out # output_string "<wef:entry\n";
		 ( match entry.e_start with
		       None -> ()
		     | Some x ->
			 out_indent out (level+4);
			 out # output_string ("start-time='" ^ 
					      Time.to_string x ^ "'\n");
		 );
		 ( match entry.e_end with
		       None -> ()
		     | Some x ->
			 out_indent out (level+4);
			 out # output_string ("end-time='" ^ 
					      Time.to_string x ^ "'\n");
		 );
		 ( match entry.e_duration with
		       None -> ()
		     | Some x ->
			 out_indent out (level+4);
			 out # output_string ("duration='" ^ 
					      Interval.to_string x ^ "'\n");
		 );
		 out_indent out (level+4);
		 out # output_string ("project='" ^ encode entry.e_project ^
				      "'\n");
		 out_indent out (level+4);
		 out # output_string ("description='" ^ 
				      encode entry.e_description ^ "'/>\n");
	      )
	      day.d_entries;

	    out_indent out (level+2);
	    out # output_string "</wef:day>\n";
	 )
	 dates;

       iter_months
         start_date
         end_date
         (fun year month ->
           let appr = Db.Approval.get db inst.i_name ~year ~month in
           match appr.a_approved with
             | None ->
                 (* do not dump missing approvals! *)
                 ()
             | Some (ts, user) ->
                 let m = Printf.sprintf "%04d-%02d" year month in
                 out_indent out (level+2);
                 out # output_string ("<wef:approval\n");
                 out_indent out (level+3);
                 out # output_string ("month='" ^ m ^ "'\n");
                 out_indent out (level+3);
                 out # output_string ("approved='yes'\n");
                 out_indent out (level+3);
                 out # output_string ("by='" ^ encode (string_of_user_name user) ^ "'\n");
                 out_indent out (level+3);
                 out # output_string ("at='" ^ Timestamp.to_iso_string ~utc:true ts ^ "'/>\n");
         );

       out_indent out (level+1);
       out # output_string "</wef:instance>\n";

    )
    inst_list;
  
  out_indent out level;
  out # output_string "</wef:instances>\n"

;;


let output_instanceLogs ?(level = 0) ?users ?instances
                        ?(start_date = very_early)
                        ?(end_date = very_late) db out =
  let inst_list =
    match instances with
	None -> Db.Instance.list db
      | Some l -> List.map (Db.Instance.get db) l
  in

  out_indent out level;
  out # output_string "<wef:instanceLogs";
  if level = 0 then
    out # output_string (" xmlns:wef='" ^ wef_namespace ^ "'>\n")
  else
    out # output_string ">\n";

  List.iter
    (fun inst ->
       ( match inst.i_name with
	     Instance n ->
	       out_indent out (level+1);
               out # output_string ("<wef:instanceLog\n");
	       out_indent out (level+2);
               out # output_string ("name='" ^ encode n ^ "'>\n");
       );

       let changes = Db.Entry.changes db inst.i_name start_date end_date in
       List.iter
         (fun change ->
            out_indent out (level+2);
            out # output_string ("<wef:change\n");
            out_indent out (level+3);
            out # output_string ("date='" ^ Date.to_string change.mc_change.d_day ^ "'\n");
            out_indent out (level+3);
            out # output_string ("by='" ^ encode (string_of_user_name change.mc_login) ^ "'\n");
            out_indent out (level+3);
            out # output_string ("at='" ^ Timestamp.to_iso_string ~utc:true change.mc_timestamp ^ "'>\n");

            List.iter
	      (fun entry ->
		 out_indent out (level+3);
		 out # output_string "<wef:entry\n";
		 ( match entry.e_start with
		       None -> ()
		     | Some x ->
			 out_indent out (level+4);
                         out # output_string ("start-time='" ^
					      Time.to_string x ^ "'\n");
		 );
		 ( match entry.e_end with
		       None -> ()
		     | Some x ->
			 out_indent out (level+4);
                         out # output_string ("end-time='" ^
					      Time.to_string x ^ "'\n");
		 );
		 ( match entry.e_duration with
		       None -> ()
		     | Some x ->
			 out_indent out (level+4);
                         out # output_string ("duration='" ^
					      Interval.to_string x ^ "'\n");
		 );
		 out_indent out (level+4);
		 out # output_string ("project='" ^ encode entry.e_project ^
				      "'\n");
		 out_indent out (level+4);
                 out # output_string ("description='" ^
				      encode entry.e_description ^ "'/>\n");
	      )
              change.mc_change.d_entries;

	    out_indent out (level+2);
            out # output_string "</wef:change>\n";
	 )
         changes;

       out_indent out (level+1);
       out # output_string "</wef:instanceLog>\n";

    )
    inst_list;

  out_indent out level;
  out # output_string "</wef:instanceLogs>\n"

;;

let export_users ?users db out =
  output_decl "wef:users" out;
  output_users ~level:0 ?users db out
;;

let export_instances ?instances ?start_date ?end_date db out =
  output_decl "wef:instances" out;
  output_instances ~level:0 ?instances ?start_date ?end_date db out
;;

let export_instanceLogs ?instances ?start_date ?end_date db out =
  output_decl "wef:instances" out;
  output_instanceLogs ~level:0 ?instances ?start_date ?end_date db out
;;

let export_dataset ?users ?instances ?start_date ?end_date db out =
  output_decl "wef:dataset" out;
  out # output_string ("<wef:dataset xmlns:wef='" ^ wef_namespace ^ "'>\n");
  output_users ~level:1 ?users db out;
  output_instances ~level:1 ?users ?instances ?start_date ?end_date db out;
  output_instanceLogs ~level:1 ?users ?instances ?start_date ?end_date db out;
  out # output_string "</wef:dataset>\n";
;;


type conflict_strategy =
  [ `Fail
  | `Overwrite
  | `Only_add
  ]
;;

exception Conflict of string
;;

let appr_month_re = Netstring_str.regexp {|^\([0-9][0-9][0-9][0-9]\)-\([0-9][0-9]\)$|}

let import ?(onconflict = `Fail) db inp =

  let rec trav_dataset node =
    assert(node # node_type = T_element "wef:dataset");
    node # iter_nodes trav_start

  and trav_users node =
    assert(node # node_type = T_element "wef:users");
    node # iter_nodes trav_user

  and trav_user node =
    assert(node # node_type = T_element "wef:user");
    let name = node # required_string_attribute "name" in
    let description = node # required_string_attribute "description" in
    let password = node # optional_string_attribute "password" in
    let email = node # required_string_attribute "email" in
    let managers = node # required_string_attribute "managers" in
    let login = node # required_string_attribute "login" = "yes" in
    let admin = node # required_string_attribute "admin" = "yes" in
    let user = { u_name = User name;
		 u_description = description;
		 u_password = password;
		 u_login = login;
                 u_admin = admin;
                 u_email = email;
                 u_managers =
                   managers
                   |> String.split_on_char ','
                   |> List.filter (fun s -> s <> "")
                   |> List.map (fun s -> User s);
	       } in
    ( try
	let old_user = Db.User.get db (User name) in
	(* User exists! *)
	if old_user <> user then (
	  match onconflict with
	      `Fail -> 
		raise (Conflict("User " ^ name))
	    | `Overwrite ->
		Db.User.update db user
	    | `Only_add ->
		()
	)
      with
	  No_such_user _ ->
	    Db.User.insert db user
    )

  and trav_instances node =
    assert(node # node_type = T_element "wef:instances");
    node # iter_nodes trav_instance

  and trav_instance node =
    assert(node # node_type = T_element "wef:instance");
    let name = node # required_string_attribute "name" in
    let description = node # required_string_attribute "description" in
    let inst = { i_name = Instance name;
		 i_description = description } in
    let inst' =
      ( try
	  let old_inst = Db.Instance.get db (Instance name) in
	  (* Instance exists! *)
	  if old_inst <> inst then (
	    match onconflict with
		`Fail -> 
		  raise (Conflict("Instance " ^ name))
	      | `Overwrite ->
		  Db.Instance.update db inst;
		  inst
	      | `Only_add ->
		  old_inst
	  )
	  else inst
	with
	    No_such_instance _ ->
	      Db.Instance.insert db inst;
	      inst
      ) in
    node # iter_nodes (trav_perm_or_day_or_approval inst')

  and trav_perm_or_day_or_approval inst node =
    match node # node_type with
	T_element "wef:permission" -> 
	  trav_permission inst node
      | T_element "wef:day" ->
          trav_day inst node
      | T_element "wef:approval" ->
          trav_approval inst node
      | _ ->
	  assert false

  and trav_permission inst node =
    assert(node # node_type = T_element "wef:permission");
    node # iter_nodes (trav_allow inst)

  and trav_allow inst node =
    assert(node # node_type = T_element "wef:allow");
    let uname = node # required_string_attribute "user" in
    let right = 
      match node # required_string_attribute "right" with
	  "read" -> `Read
	| "write" -> `Write
	| "owner" -> `Owner
	| _ -> assert false in
    let pset = Db.Permission.get db inst.i_name in
    if List.mem_assoc (User uname) pset.p_set then (
      (* There is already a right for this user *)
      let old_right = List.assoc (User uname) pset.p_set in
      if old_right <> right then (
	match onconflict with
	    `Fail -> 
	      raise (Conflict("Instance " ^ string_of_inst_name inst.i_name ^
			      ", permission of user " ^ uname))
	  | `Overwrite ->
	      let pset' =
		{ p_instance = inst.i_name;
		  p_set =
		    List.map
		      (fun (u,r) ->
			 if u = User uname then
			   (u, right)
			 else
			   (u, r)
		      )
		      pset.p_set
		} in
	      Db.Permission.update db pset'
	  | `Only_add ->
	      ()
      )
    ) else (
      let pset' =
	{ p_instance = inst.i_name;
	  p_set = (User uname, right) :: pset.p_set
	} in
      Db.Permission.update db pset'
    )

  and trav_approval inst node =
    assert(node # node_type = T_element "wef:approval");
    let approved = node # required_string_attribute "approved" in
    if approved = "yes" then (
      let month_str = node # required_string_attribute "month" in
      let year, month =
        match Netstring_str.string_match appr_month_re month_str 0 with
          | Some res ->
              let year =
                Netstring_str.matched_group res 1 month_str
                |> int_of_string in
              let month =
                Netstring_str.matched_group res 2 month_str
                |> int_of_string in
              (year, month)
          | None ->
              failwith ("cannot parse month: " ^ month_str) in
      let by_str = node # required_string_attribute "by" in
      let at_str = node # required_string_attribute "at" in
      let at = Timestamp.from_iso_string ~utc:true at_str in
      let appr =
        { a_instance = inst.i_name;
          a_year = year;
          a_month = month;
          a_approved = Some (at, User by_str)
        } in
      Db.Approval.update db appr
    )

  and trav_day inst node =
    assert(node # node_type = T_element "wef:day");
    let date_str = node # required_string_attribute "date" in
    let date = Date.from_string date_str in
    let entries_as_tuples =
      Array.of_list
	(List.map
           (trav_entry date inst.i_name)
	   (node # sub_nodes)) in
    let entries =
      Array.init (Array.length entries_as_tuples)
	(fun k ->
	   let (start_time, end_time, duration, project, description) =
	     entries_as_tuples.(k) in
	   { e_id = None;
	     e_instance = inst.i_name;
	     e_day = date;
	     e_index = k;
	     e_start = start_time;
	     e_end = end_time;
	     e_duration = duration;
	     e_project = project;
	     e_description = description;
	   }
	) in

    let old_day = Db.Entry.get db inst.i_name date in
    if old_day.d_entries <> [] then (
      (* Compare old_entries and entries: *)
      let old_entries' =
	Array.of_list
	  (List.map
	     (fun e ->
		{ e with e_id = None }) old_day.d_entries) in
      if old_entries' <> entries then (
	match onconflict with
	    `Fail -> 
	      raise (Conflict("Instance " ^ string_of_inst_name inst.i_name ^
			      ", date " ^ date_str))
	  | `Overwrite ->
	      let day =
		{ d_instance = inst.i_name;
		  d_day = date;
		  d_entries = Array.to_list entries;
		} in
	      Db.Entry.update db day
	  | `Only_add ->
	      ()
      )
    ) else (
      let day =
	{ d_instance = inst.i_name;
	  d_day = date;
	  d_entries = Array.to_list entries;
	} in
      Db.Entry.update db day
    )

  and trav_entry date inst_name node =
    assert(node # node_type = T_element "wef:entry");
    let start_time = node # optional_string_attribute "start-time" in
    let end_time = node # optional_string_attribute "end-time" in
    let duration = node # optional_string_attribute "duration" in
    let project = node # required_string_attribute "project" in
    let description = node # required_string_attribute "description" in

    ((match start_time with
	  Some d -> Some(Time.from_string d) | None -> None),
     (match end_time with
	  Some d -> Some(Time.from_string d) | None -> None),
     (match duration with
	  Some d -> Some(Interval.from_string d) | None -> None),
     project, 
     description)

  and trav_instanceLogs node =
    assert(node # node_type = T_element "wef:instanceLogs");
    node # iter_nodes trav_instanceLog

  and trav_instanceLog node =
    assert(node # node_type = T_element "wef:instanceLog");
    let name = node # required_string_attribute "name" in
    ( try
        let _inst = Db.Instance.get db (Instance name) in
        ()
      with
        | No_such_instance _ ->
            raise (Conflict ("InstanceLog " ^ name))
    );
    node # iter_nodes (trav_change (Instance name))

  and trav_change inst node =
    assert(node # node_type = T_element "wef:change");
    let date_str = node # required_string_attribute "date" in
    let date = Date.from_string date_str in
    let login = User (node # required_string_attribute "by") in
    let at_str = node # required_string_attribute "at" in
    let at = Timestamp.from_iso_string ~utc:true at_str in
    let entries_as_tuples =
      Array.of_list
	(List.map
	   (trav_entry date inst)
	   (node # sub_nodes)) in
    let entries =
      Array.init (Array.length entries_as_tuples)
	(fun k ->
	   let (start_time, end_time, duration, project, description) =
	     entries_as_tuples.(k) in
	   { e_id = None;
             e_instance = inst;
	     e_day = date;
	     e_index = k;
	     e_start = start_time;
	     e_end = end_time;
	     e_duration = duration;
	     e_project = project;
	     e_description = description;
	   }
	) in
    let day =
      { d_instance = inst;
        d_day = date;
        d_entries = Array.to_list entries
      } in
    Db.Entry.log ~at db login day

  and trav_start node =
    match node # node_type with
	T_element "wef:dataset" -> 
	  trav_dataset node
      | T_element "wef:users" ->
	  trav_users node
      | T_element "wef:instances" ->
	  trav_instances node
      | T_element "wef:instanceLogs" ->
          trav_instanceLogs node
      | _ ->
	  failwith "Invalid XML starting point"
  in

  let m = Pxp_dtd.create_namespace_manager() in
  let config =
    { default_config with
      (* warner = XXX; *)  (* TODO *)
      enable_namespace_processing = Some m;
      encoding = Const.internal_charset;
      recognize_standalone_declaration = false;
    } in

  let toplevel_id = Private(allocate_private_id()) in

  let wef_dtd_file = Filename.concat Const.ui_dir "wef.dtd" in

  let resolver =
    new combine
      [ lookup_public_id_as_file 
	  [ wef_public_id, wef_dtd_file ];
	lookup_system_id_as_file
	  [ wef_system_id, wef_dtd_file ];
	new resolve_as_file ();
	new resolve_read_this_channel ~id:toplevel_id inp;
      ] in

  let doc = 
    parse_document_entity
      config
      (ExtID(toplevel_id, resolver))
      default_namespace_spec
  in

  trav_start doc#root
;;
