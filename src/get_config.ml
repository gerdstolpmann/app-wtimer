(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


type config = Pxp_dtd.dtd

let parse() =
  let wtimer_config_xml =
    Filename.concat Const.ui_dir "wtimer-config.xml" in
  let pxp_config =
    { Pxp_yacc.default_config with
	Pxp_yacc.encoding = Const.internal_charset;
    } in
  Pxp_yacc.parse_dtd_entity pxp_config (Pxp_yacc.from_file wtimer_config_xml)
;;


let option dtd name =
  try
    let (e,_) = dtd # gen_entity name in
    Pxp_dtd.Entity.replacement_text e
  with
      _ ->
	failwith ("Cannot find configuration option " ^ name)
;;


let int_option dtd name =
  let v = option dtd name in
  try int_of_string v
  with _ ->
    failwith ("Cannot parse configuration option " ^ name ^ " as integer number")
;;


let bool_option dtd name =
  let v = option dtd name in
  match v with
      "yes" | "true" -> true
    | "no" | "false" -> false
    | _ ->
	failwith ("Cannot parse configuration option " ^ name ^ " as bool")
;;


let ws_re = Netstring_pcre.regexp "[ \t\r\n]+";;

let list_option dtd name =
  let v = option dtd name in
  Netstring_pcre.split ws_re v
;;
