(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

open Wd_types

let new_startpage =
  ref (fun _ _ -> assert false
	 : universe_type -> environment -> dialog_type) ;;

let new_session =
  ref (fun _ _ -> assert false
	 : universe_type -> environment -> dialog_type) ;;

let new_editor =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

let new_timetravel =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

let new_export =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

let new_selectuser =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

let new_admin =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

let new_admin_password =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

let new_admin_accounts =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

let new_admin_access =
  ref (fun _ _ _ -> assert false
	 : universe_type -> environment -> dialog_type option -> dialog_type) ;;

