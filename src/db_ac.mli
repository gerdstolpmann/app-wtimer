(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)


(* This module provides the same functions as Db, but with access control.
 * All functions have an extra argument [login_name] that determines the
 * user who performes the operation.
 *)

(* Terms:
 * - "Unprivileged user": A user who is not administrator
 *)

module Types : sig
  type login_name = Db.Types.user_name
  exception Permission_denied
end

module User : sig
  open Types
  open Db.Types
  open Db.Connection
  val list : connection -> login_name -> user list
    (* Currently there are no restrictions *)

  val get : connection -> login_name -> user_name -> user
    (* Currently there are no restrictions *)

  val exists : connection -> login_name -> user_name -> bool
    (* Currently there are no restrictions *)

  val insert : connection -> login_name -> user -> unit
    (* Restrictions: Only administrators can create users.
     *)

  val update : connection -> login_name -> user -> unit
    (* Restrictions: Unprivileged users cannot set the admin flag
     * of user accounts. Unprivileged users can only modify themselves.
     *)

  val delete : connection -> login_name -> user_name -> unit
    (* Restrictions: Unprivileged users can only delete themselves.
     *)
end

module Instance : sig
  open Types
  open Db.Types
  open Db.Connection

  val list : connection -> login_name -> instance list
    (* There are currently no restrictions *)

  val get : connection -> login_name -> inst_name -> instance
    (* There are currently no restrictions *)

  val exists : connection -> login_name -> inst_name -> bool
    (* There are currently no restrictions *)

  val insert : connection -> login_name -> instance -> unit
    (* There are currently no restrictions.
     * This function also adds the permission that the instance
     * is owned by the login user. This can be changed later.
     *)

  val update : connection -> login_name -> instance -> unit
    (* Restrictions: Unprivileged users can only modify instances
     * they own.
     *)

  val delete : connection -> login_name -> inst_name -> unit
    (* Restrictions: Unprivileged users can only delete instances
     * they own.
     *)
end

module Permission : sig
  open Types
  open Db.Types
  open Db.Connection

  (* check: omitted *)

  val get : connection -> login_name -> inst_name -> perm_set
    (* Restrictions: Unprivileged users get only permission sets
     * of instances they own.
     *)

  val update : connection -> login_name -> perm_set -> unit
    (* Restrictions: Unprivileged users can only set permission sets
     * of instances they own.
     *)
end

module Approval : sig
  open Types
  open Db.Types
  open Db_types.Types
  open Db.Connection

  val get : connection -> login_name -> inst_name -> year:int -> month:int -> approval
  val approve : connection -> login_name -> inst_name -> year:int -> month:int -> unit
end

module Entry : sig
  open Types
  open Db.Types
  open Db_types.Types
  open Db.Connection

  val list : connection -> login_name -> inst_name -> date -> date -> date list
    (* Restrictions: Users get only entries of instances
     * for which they have at least read privilege.
     * The admin user does not have more privileges than other users.
     *)

  val changes : connection -> login_name -> inst_name -> date -> date -> day change list
    (* Restrictions: Users get only entries of instances
     * for which they have at least read privilege.
     * The admin user does not have more privileges than other users.
     *)

  val get : connection -> login_name -> inst_name -> date -> day
    (* Restrictions: Users get only entries of instances
     * for which they have at least read privilege.
     * The admin user does not have more privileges than other users.
     *)

  val update : connection -> login_name -> day -> unit
    (* Restrictions: Users can only modify entries of instances
     * for which they have at least write privilege.
     * The admin user does not have more privileges than other users.
     *)

  val projects : connection -> login_name -> inst_name -> date -> date -> string list
    (* Restrictions: Users get only entries of instances
     * for which they have at least read privilege.
     * The admin user does not have more privileges than other users.
     *)

end
