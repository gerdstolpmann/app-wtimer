(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

module Types = struct
  type login_name = Db.Types.user_name
  exception Permission_denied
end


module User = struct
  open Types
  open Db.Types
  open Db.Connection

  let list db login = Db.User.list db

  let get db login name = Db.User.get db name

  let exists db login name = Db.User.exists db name

  let insert db login user =
    let luser = Db.User.get db login in
    if not luser.u_admin then raise Permission_denied;
    Db.User.insert db user

  let update db login user =
    let luser = Db.User.get db login in
    if not luser.u_admin && login <> user.u_name then raise Permission_denied;
    if not luser.u_admin && user.u_admin then raise Permission_denied;
    Db.User.update db user

  let delete db login name =
    let luser = Db.User.get db login in
    if not luser.u_admin && login <> name then raise Permission_denied;
    Db.User.delete db name
end


module Instance = struct
  open Types
  open Db.Types
  open Db.Connection

  let list db login = Db.Instance.list db

  let get db login name = Db.Instance.get db name

  let exists db login name = Db.Instance.exists db name

  let insert db login inst =
    Db.Instance.insert db inst;
    Db.Permission.update db { p_instance = inst.i_name;
			      p_set = [ login, `Owner ];
			    }

  let update db login inst =
    let luser = Db.User.get db login in
    if not luser.u_admin &&
       not (Db.Permission.check db inst.i_name login `Owner) then
	 raise Permission_denied;
    Db.Instance.update db inst

  let delete db login name =
    let luser = Db.User.get db login in
    if not luser.u_admin &&
       not (Db.Permission.check db name login `Owner) then
	 raise Permission_denied;
    Db.Instance.delete db name

end


module Permission = struct
  open Types
  open Db.Types
  open Db.Connection

  let get db login name =
    let luser = Db.User.get db login in
    if not luser.u_admin &&
       not (Db.Permission.check db name login `Owner) then
	 raise Permission_denied;
    Db.Permission.get db name

  let update db login p =
    let luser = Db.User.get db login in
    if not luser.u_admin &&
       not (Db.Permission.check db p.p_instance login `Owner) then
	 raise Permission_denied;
    Db.Permission.update db p

end


module Approval = struct
  open Types
  open Db.Types
  open Db_types.Types
  open Db.Connection

  let get db login sheet ~year ~month =
    if not (Db.Permission.check db sheet login `Read) then
      raise Permission_denied;
    Db.Approval.get db sheet ~year ~month

  let approve db login sheet ~year ~month =
    if not (Db.Permission.check_month_writable db sheet login ~year ~month) then
      raise Permission_denied;
    Db.Approval.approve db login sheet ~year ~month

end

module Entry = struct
  open Types
  open Db.Types
  open Db_types.Types
  open Db.Connection

  let list db login name from_date to_date =
    if not (Db.Permission.check db name login `Read) then
      raise Permission_denied;
    Db.Entry.list db name from_date to_date

  let changes db login name from_date to_date =
    if not (Db.Permission.check db name login `Read) then
      raise Permission_denied;
    Db.Entry.changes db name from_date to_date

  let projects db login name from_date to_date =
    if not (Db.Permission.check db name login `Read) then
      raise Permission_denied;
    Db.Entry.projects db name from_date to_date

  let get db login name date =
    if not (Db.Permission.check db name login `Read) then
      raise Permission_denied;
    Db.Entry.get db name date

  let update db login day =
    let d = Db_types.Date.access day.d_day in
    let year = d.year in
    let month = d.month in
    if not (Db.Permission.check_month_writable db day.d_instance login ~year ~month) then
      raise Permission_denied;
    Db.Entry.update db day;
    Db.Entry.log db login day
end
