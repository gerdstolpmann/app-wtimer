(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

module Types : sig
  type date
    (* dates with resolution of 1 day *)

  type date_rec = { year : int;    (* 4 digits *)
		    month : int;   (* 1-12 *)
		    mday : int;    (* 1-31 *)
		  }
    (* date record *)

  type time
    (* time of day, computations are modulo 24 hours *)

  type time_rec = { hour : int;    (* 0-23 *)
		    minute : int;  (* 0-59 *)
		  }
    (* time record *)		    

  type interval
    (* differences between time values *)

  type interval_rec = { delta_hours   : int;
			delta_minutes : int; (* abs value: 0-59 *)
		      }
    (* Interval record *)
  (* Note: Either both delta_hours and delta_minutes are non-negative,
   * or they are both negative.
   *)

  type timestamp = int64
  (* milliseconds since the epoch *)

  exception Bad_date
  exception Bad_time
  exception Bad_interval
    (* Raised by the "create" functions below if the passed argument is
     * invalid.
     *)
end

module Date : sig
  open Types
  val invalid : date
  (* an invalid date *)

  val create : date_rec -> date
    (* Create a valid date, or raise Bad_date *)

  val access : date -> date_rec
    (* Open the components of a date, e.g. (access t).year *)

  val to_string : date -> string
    (* Format as ISO date string: YYYY-MM-DD *)

  val from_string : string -> date
    (* Parse an ISO date string *)

  val cmp : date -> date -> int
    (* cmp t1 t2 = 0: t1 and t2 are the same,
     * cmp t1 t2 > 0: t1 later than t2,
     * cmp t1 t2 < 0: t1 earlier than t2 
     *)

  val days_of_month : date -> int
    (* Returns the number of days of the month mentioned in the date *)

  val week_day : date -> int
    (* week day, from 0 (Sunday) to 6 (Saturday) *)

  val iso8601_week_pair : date -> int * int
    (* Returns [(week_number, year)] for the ISO-8601 definition of weeks.
       The week starts with Monday, and has numbers 1-53. A week is considered
       to be part of the year into which four or more days fall.
     *)

end

module Time : sig
  open Types
  val create : time_rec -> time
    (* Create a valid time value, or raise Bad_time *)

  val access : time -> time_rec
    (* Open the components of a time value, e.g. (access t).hour *)

  val to_string : time -> string
    (* Format as ISO time string: HH:MM *)

  val from_string : string -> time
    (* Parse an ISO time string *)

  val cmp : time -> time -> int
    (* cmp t1 t2 = 0: t1 and t2 are the same,
     * cmp t1 t2 > 0: t1 later than t2,
     * cmp t1 t2 < 0: t1 earlier than t2 
     *)
end

module Interval : sig
  open Types
  val create : interval_rec -> interval
    (* Create a valid time interval, or raise Bad_interval *)

  val access : interval -> interval_rec
    (* Open the components of an interval, e.g. (access t).delta_minutes *)

  val to_string : interval -> string
    (* Format as ISO time string: H:MM or -H:MM *)

  val from_string : string -> interval
    (* Parse an ISO time string *)

  val cmp : interval -> interval -> int
    (* cmp t1 t2 = 0: t1 and t2 are the same,
     * cmp t1 t2 > 0: t1 longer interval than t2,
     * cmp t1 t2 < 0: t1 shorter interval than t2 
     *)

  val add : interval -> interval -> interval
    (* The sum of two intervals *)

  val neg : interval -> interval
    (* The negative interval of the argument *)

  val delta : time -> time -> interval   
    (* t1 - t2 *)

  val move_by : time -> interval -> time     
    (* t1 + delta modulo 24 hours *)

  val distance : time -> time -> interval
    (* t1 - t2 mod 24 hours *)
end

module Timestamp : sig
  open Types
  val from_epoch_string : string -> timestamp

  val to_epoch_string : timestamp -> string
    (* returns the timestamp as seconds since the epoch, formatted with
       exactly three fractional digits
     *)

  val to_date : timestamp -> date
  (* return the date part *)

  val from_iso_string : ?utc:bool -> string -> timestamp
  val to_iso_string : ?utc:bool -> timestamp -> string
  (* return as YYYY-MM-DD HH:MM:SS.FFFFFF *)
end
