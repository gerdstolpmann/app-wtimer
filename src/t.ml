(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

#use "db.ml";;
open Types;;
let c = Connection.connect ();;
let d1 = { year = 2000; month = 6; mday = 22 };;
let d2 = { year = 2000; month = 6; mday = 23 };;
let npc = Instance "Stunden NPC";;
let day =
  { d_instance = npc;
    d_day = d1;
    d_entries = 
      [ { e_id = None;
	  e_instance = npc;
	  e_day = d1;
	  e_index = 0;
	  e_start = Some { hour = 8; minute = 30 };
	  e_end   = Some { hour = 12; minute = 45 };
	  e_duration = Some { delta_hours = 4; delta_minutes = 15 };
	  e_project = "Phantasy";
	  e_description = "Worked hard for nothing";
	  };
	
	{ e_id = None;
	  e_instance = npc;
	  e_day = d1;
	  e_index = 1;
	  e_start = Some { hour = 14; minute = 00 };
	  e_end   = Some { hour = 15; minute = 45 };
	  e_duration = Some { delta_hours = 49; delta_minutes = 15 };
	  e_project = "Phantasy";
	  e_description = "Worked hard for nothing";
	  };

	{ e_id = None;
	  e_instance = npc;
	  e_day = d1;
	  e_index = 2;
	  e_start = Some { hour = 16; minute = 30 };
	  e_end   = Some { hour = 16; minute = 45 };
	  e_duration = Some { delta_hours = 0; delta_minutes = 15 };
	  e_project = "Phantasy";
	  e_description = "Worked hard for nothing";
	  };
	
      ]
  }
;;
