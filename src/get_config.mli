(*
 * <COPYRIGHT>
 * Copyright 2003 Gerd Stolpmann
 *
 * <GPL>
 * This file is part of WTimer.
 *
 * WTimer is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * WTimer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WDialog; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * </>
 *)

(* $Id$
 * ----------------------------------------------------------------------
 *
 *)

type config
  (* The type representing the parsed configuration file *)

val parse : unit -> config
  (* Read the configuration file *)

val option : config -> string -> string
  (* Returns the value of the passed option *)

val int_option : config -> string -> int
  (* Returns the value of the passed option as int. Fails if the option
   * is not an int.
   *)

val bool_option : config -> string -> bool
  (* Returns the value of the passed option as bool. Fails if the option
   * is not a bool ("yes"/"no").
   *)

val list_option : config -> string -> string list
  (* Returns the value of the passed option as string list. The members
   * are separated by spaces. 
   *)

